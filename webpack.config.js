var webpack = require('webpack');
module.exports={
    entry: [
        'script!jquery/dist/jquery.min.js',
        './app/app.jsx'
    ],
    externals:{
        jquery:'jQuery'
    },
    plugins:[
        new webpack.ProvidePlugin({
            '$':'jquery',
            'jQuery':'jquery'
        })
    ],
    output: {
        path: __dirname,
        filename:'./public/bundle.js'
    },
    resolve:{
        root:__dirname,
        alias:{
            LoginForm: 'app/component/LoginForm.jsx',
            Forgot: 'app/component/Forgot.jsx',
            LoginMessage: 'app/component/LoginMessage.jsx',
            Login: 'app/component/Login.jsx',
            Dashboard: 'app/component/Dashboard.jsx',
            NavSideBar: 'app/component/NavSideBar.jsx',
            Analytics: 'app/component/Analytics.jsx',
            TopNav: 'app/component/TopNav.jsx',
            Review: 'app/component/Review.jsx',
            Customer: 'app/component/Customer.jsx',
            Reset: 'app/component/Reset.jsx',
            Settings: 'app/component/Settings.jsx',
            Online: 'app/component/Online.jsx',
            Private: 'app/component/Private.jsx',
            Account: 'app/component/Account.jsx',
            TeamM: 'app/component/Team.jsx',
            CustomSms: 'app/component/Sms.jsx',
            LocationDropDown: 'app/component/LocationDropDown.jsx',
            loginApi:'app/api/loginApi.jsx',
            invitationApi:'app/api/invitationApi.jsx',
            reviewApi:'app/api/reviewApi.jsx',
            locationDropDownApi:'app/api/locationDropDownApi.jsx',
            getCurrentUser:'app/api/getCurrentUser.jsx',
            LocationStats:'app/api/LocationStats.jsx',

        },
        extensions:['','.js','.jsx']
    },
    module:{
        loaders:[
            {
                loader: 'babel-loader',
                query: {
                    presets: ['react', 'es2015']
                },
                test: /\.jsx?$/,
                exclude: /(node_modules|bower_component)/
            }
        ]

    },
    devtool:'cheap-module-eval-source-map'
};