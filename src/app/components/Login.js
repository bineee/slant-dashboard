import React from "react";

export class Login extends React.Component {
    render() {
        return(
            <div className="container log_body">
                <div className="row emp">

                </div>
                <div className="row">
                    <div className="col-md-4 col-md-offset-4">
                        <form className="v_center">
                            <div className="box">
                                <div className="row">
                                    <div className="col-md-4 col-md-offset-4 box_inside">
                                        <div className="row">
                                            <div className="col-md-4 col-md-offset-4 reviews_await">
                                                Reviews await.
                                            </div>
                                        </div>
                                        <br/><br/>
                                        <input type="text" placeholder="Username" id="log_user" className="log_user" autoFocus required/>
                                        <input type="password" placeholder="Password" id="log_pass" className="log_pass" required/>
                                        <button className="btn btn-primary btn_signin">
                                            <img src="images/airplane.png"/>&#8195; Sign In
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        );
    }
}