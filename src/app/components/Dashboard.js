import React from "react";
import {Navbar} from "./Navsidebar";
import {Analytic} from "./Analytic";

export class Dashboard extends React.Component {
    render() {
        return(

            <div className="container anly_body">
                <div className="row">

                    <Navbar/>

                    <Analytic/>

                </div>
            </div>
        );
    }
}