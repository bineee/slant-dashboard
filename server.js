// var express = require('express');
//
// //Create our app
// var app = express();
//
// app.use(express.static('public'));
// app.listen(3999, function () {
//     console.log('Express server is up on port 3000');
// });

const express = require('express');
const path = require('path');
const port = process.env.PORT || 80;

const app = express();


app.use(function (req, res, next) {
    if(req.header('x-forwarded-proto') === 'https'){
        res.redirect('http://'+req.hostname+req.url);
    }else{
        next();
    }
})
app.use(express.static(__dirname + '/public'));

// handle every other route with index.html, which will contain
// a script tag to your application's JavaScript file(s).


    app.get('/reset/:id', function(req, res) {
        const url = require('url');
        if(typeof (req.params.id) === 'undefined' || typeof req.query.email=== 'undefined'){
            res.redirect(url.format({pathname:"/login"}))
        }
        var uniqueCode = req.params.id;
        var emailId = req.query.email;


        res.redirect(url.format({
            pathname:"/reset",
            query: {
                "unique_code":uniqueCode,
                "email": emailId,
            }
        }));
    });
    /*console.log(request.query.q);
    var emailId = request.params;
    /!*response.sendFile(path.resolve(__dirname, 'public', 'index.html'))*!/
    response.redirect('http://'+request.hostname+':4999/reset');*/
app.get('*', function (request, response){
    response.sendFile(path.resolve(__dirname, 'public', 'index.html'))
})

app.listen(port)
console.log("server started on port" + port)