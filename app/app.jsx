// React Component
//import {Settings} from "./component/Settings";

var React = require('react');
var ReactDOM = require('react-dom');
var {Route, Router, IndexRoute, hashHistory,browserHistory} = require('react-router');
var Dashboard = require('Dashboard');
var Login = require('Login');
var Review = require('Review');
var Analytics = require('Analytics');
var Customer = require('Customer');
var Settings = require('Settings');
var Reset = require('Reset');
var Forgot = require('Forgot');

var Online = require('Online');
var Private = require('Private');
var Account = require('Account');
var TeamM = require('TeamM');
var CustomSms = require('CustomSms');

var Cookies = require('universal-cookie');
const cookies = new Cookies();
//Load foundation

function requireAuth(nextState, replace) {
    if (!cookies.get('token') || typeof(cookies.get('token')) === undefined || cookies.get('token') === '') {
        replace({
            pathname: '/login'
        })
    }
}

/*ReactDOM.render(
    <Dashboard/>,
        document.getElementById("app")

);*/
ReactDOM.render(
    <Router history={browserHistory}>
        <Route path={"/login"} component={Login} pattern="/login"/>
        <Route path={"/reset"} component={Reset} pattern ="/reset"/>
        <Route path={"/forgot"} component={Forgot} pattern ="/forgot"/>
        <Route path={"/"} component={Dashboard} onEnter={requireAuth}>
            <IndexRoute component={Analytics}/>
            <Route component={Analytics} path="analytics" pattern="/analytics"  />
            <Route component={Review} path="review" pattern="/review"  />
            <Route exactly component={Customer} path="customer" pattern="/customer" />
            <Route component={Settings} path="settings" pattern="/settings" />
            <Route component={Online} path="online" pattern="/online" />
            <Route component={Private} path="private" pattern="/private" />
            <Route component={Account} path="account" pattern="/account" />
            <Route component={TeamM} path="team" pattern="/team" />
            <Route component={CustomSms} path="sms" pattern="/sms" />
            {/*<Route exactly component={Page3} pattern="/path3" />*/}
        </Route>

    </Router>,
    document.getElementById('app')
);




