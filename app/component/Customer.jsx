var React = require('react');
var Cookies = require('universal-cookie');
var moment = require('moment');
var Modal = require('react-modal');
var axios = require('axios');
var invitationApi = require('invitationApi');
var cookies = new Cookies();
var loadPosition = {
    top: 'unset !important',
    margin: '5% 47%'
};

const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)'
    }
};


var Customer = React.createClass({
    getInitialState: function () {
        return this.state = {
            isActive: false,
            customerData: this.props.customerJson,
            search: '',
        }
    },
    componentWillMount() {
        Modal.setAppElement('body');
    },
    componentWillReceiveProps: function (nextProps) {
        this.setState({customerData: this.props.customerJson});
        this.setState({search:''});
    },
    componentDidMount: function () {
        document.title = " Slant | Customer"
    },
    toggleModal: function (name, number) {
        this.setState({
            isActive: !this.state.isActive, name, number
        })
    },
    handleErrorMessage(errorMessage, type){
        this.props.handleErrorMessageInDashBoard(errorMessage, type);
    },

    sendInvitation: function (name, number, location) {
        this.setState({
            isActive: !this.state.isActive
        });
        var todayDate = new Date();
        var that = this;
        var customerArray = this.props.customerJson;
        const defaultURL = 'https://api.slantreviews.com/v2/locations/';
        axios.defaults.headers.common['Authorization'] = cookies.get('token');
        return axios.post(defaultURL + location + '/review_invitations', {
            to_number: number,
            to_name: name
        }, {}).then(function (res) {
            try {
                if (res.status === 201) {
                    var c = customerArray.review_invitations.push({
                        client_name: name,
                        client_number_natural: number,
                        date_sent: todayDate,
                        recommended: null,
                    });
                    that.setState({customerJson: c});
                    that.props.updateCustomerCount(customerArray.review_invitations.length);
                    that.handleErrorMessage("Invitation sent successfully", 'success');
                    return true;
                } else {
                    var errorMessage = "Sorry some error occured";
                    if(typeof (res.response.data) !== undefined) {
                        errorMessage = res.response.data.message;
                    }
                    that.handleErrorMessage(errorMessage, type);
                    //that.props.onErrorMessage(errorMessage, 'error')
                    return false;
                }
            }
            catch (e) {
                throw e;
            }

        });

    },
    updateSearch(event) {
        var searchValue = event.target.value;
        searchValue = searchValue.toString();
        searchValue = searchValue.toLowerCase();
        this.setState({search: searchValue});
    },

    render: function () {
        var customerJson = this.state.customerData;
        let filteredCustomer = [];
        if (customerJson != '') {
            filteredCustomer = customerJson.review_invitations.filter(
                (detail) => {
                    if (typeof(detail.client_name) !== "undefined") {
                        //  return detail.client_name.toLowerCase().indexOf(this.state.search) != -1;
                        return (
                            detail.client_name.toLowerCase().indexOf(this.state.search) >= 0 ||
                            detail.client_number_natural.toLowerCase().indexOf(this.state.search) >= 0
                            //||   item.phone.toLowerCase().indexOf(query) >= 0
                        )
                    }
                }
            );
        }


        var selectedLocation = this.props.locId;
        var customerInfo = () => {
            if (jQuery.isEmptyObject(customerJson) || typeof(customerJson) === "undefined") {

                return <tr>
                    <td className="load_style" colSpan={6}><img style={loadPosition} src="images/load_icon.gif"/></td>
                </tr>
            }
            if (filteredCustomer.length > 0) {
                return filteredCustomer.map((result, index) => {
                    var converted_date = moment.unix(result.date_sent).format("MM-DD-YYYY");
                    result.invited = converted_date;
                    return (
                        <tr>
                            <td>{result.client_name}</td>
                            <td>{result.client_number_natural}</td>
                            <td>{(result.date_clicked) ? 'Yes' : 'No'}</td>
                            <td>{(result.recommended == false) ? 'No' : (result.recommended == true) ? 'Yes' : '-'}</td>
                            <td>{result.invited}</td>
                            <td><img
                                className="change_cursor"
                                onClick={this.toggleModal.bind(this, result.client_name, result.client_number_e164)}
                                src="images/CustomerRequestIcon.png"/></td>
                        </tr>
                    )
                })
            }
            return <tr>
                <td className="no_record"> No Record Found</td>
                {/*<td className="load_style" colSpan={6}>*/}
                {/**/}
                {/*/!*<img style={loadPosition} src="images/load_icon.gif"/>*!/*/}
                {/*</td>*/}
            </tr>
        };
        return (

            <div className="bg_blu_m">

                <div className="row">
                    <div className="col-lg-8 col-xs-12 all_reviews">
                        <span className="all_review_head">Customers</span>
                        {/*<form className="form_filter">*/}
                        <input className="srch_button show-large"
                               value={this.state.search} onChange={this.updateSearch.bind(this)}
                               name="Search" placeholder="Search"/>
                        <input className="srch_button show-med" style={{ 'margin': '0 45px 0 0'}}
                               value={this.state.search} onChange={this.updateSearch.bind(this)}
                               name="Search" placeholder="Search"/>
                        {/*</form>*/}
                    </div>
                    <div className="col-lg-4">
                    </div>
                </div>

                <div className="row">
                    <div className="col-xs-12 show-mobile">
                        <div className="mob_align_center">
                            <input className="srch_button_mob"
                                   value={this.state.search} onChange={this.updateSearch.bind(this)}
                                   name="Search" placeholder="Search"/>
                        </div>
                    </div>
                </div>
                <div className="row">

                    <div className="col-lg-8 table_box">

                        <div className="table-responsive">
                            <table id="cust_table"className="table table-hover">
                                <thead>
                                <tr>
                                    <th>Customer</th>
                                    <th>Phone</th>
                                    <th>Clicked Thru</th>
                                    <th>Recommended?</th>
                                    <th>Invited</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                {customerInfo()}
                                </tbody>


                            </table>
                        </div>

                    </div>
                    <div className="col-lg-4">
                    </div>
                </div>


                <Modal
                    isOpen={this.state.isActive}
                    onRequestClose={this.toggleModal}
                    contentLabel="Example Modal"
                    className="box"
                    keyboard={true}
                    onHide={this.close}>

                    <div className="container">
                        <div className="row">
                            <div className="col-lg-4">

                            </div>
                            <div className="col-lg-4 col-xs-12 box">
                                <div className="row">
                                    <div className="col-lg-2 col-xs-1">
                                    </div>
                                    <div className="col-lg-8 col-xs-10 box_inside">
                                        <form className="reviews_customer">
                                            Send a review request to
                                            <br/>
                                            your happy customers.
                                            <br/>
                                            <br/>
                                            <input type="text" placeholder="First Name" className="log_user"
                                                   readOnly={true}
                                                   value={this.state.name}
                                                   autoFocus required/>

                                            <input type="tel" placeholder="Phone Number" className="log_pass"
                                                   readOnly={true}
                                                   value={this.state.number}
                                                   required/>

                                            <button onClick={function () {
                                                this.toggleModal();
                                                this.sendInvitation(this.state.name, this.state.number, selectedLocation);
                                            }.bind(this)} className="btn btn_primary btn_signin">
                                                <img src="images/airplane.png"/>&#8195;  Send Request
                                            </button>
                                        </form>
                                    </div>
                                    <div className="col-lg-2 col-xs-1">
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-4">

                            </div>
                        </div>
                    </div>

                </Modal>

            </div>

        )
    }
});

module.exports = Customer;