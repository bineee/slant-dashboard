var React = require('react');
import {Tab, Tabs, TabList, TabPanel} from 'react-tabs';
import StarRating from 'react-star-rating';

var moment = require('moment');
var starStyle = {
    width: '20px !important',
};


var loadPosition = {
    top: 'unset !important',
    margin: '5% 47%'
}




var OnlineReview = React.createClass({
    getInitialState: function () {
        return this.state = {
            search: '',
        }
    },
    componentWillReceiveProps: function(nextProps){
        this.setState({search:''})
    },
    componentDidMount: function () {
        document.title = "Slant | Review"
    },
    updateSearch(event) {
        var searchValue = event.target.value;
        searchValue = searchValue.toString();
        searchValue = searchValue.toLowerCase();
        this.setState({search: searchValue});
    },
    render: function () {
        var reviewData = this.props.reviewsJson;
        let filteredReview = [];
        if (reviewData != '') {
            filteredReview = reviewData.reviews.filter(
                (detail) => {
                    if (typeof(detail.author) !== "undefined" && typeof(detail.body) !== "undefined") {
                        return (
                            detail.author.toLowerCase().indexOf(this.state.search) >= 0
                            || detail.body.toLowerCase().indexOf(this.state.search) >= 0
                        )
                    }
                }
            );
        }
        var reviewInfo = () => {
            if (typeof(reviewData.reviews) === "undefined") {
                return <div className="load_style"><img style={loadPosition} src="images/load_icon.gif"/></div>
            }
            if (jQuery.isEmptyObject(reviewData.reviews) && reviewData.reviews.length === 0) {
                //return <div className="load_style"><img style={loadPosition} src="images/load_icon.gif"/></div>
                return <div className="col-lg-8 review_box">No Record found</div>
            }

            if (filteredReview.length > 0) {
                return filteredReview.map((result, index) => {
                    var fulldate = moment.unix(result.date).format("DD MMMM, YYYY");
                    var converted_date = fulldate.toString("MMMM");

                    // var today = moment().format("YYYY-MM-DD"); //2014-07-10
                    // var dateToday = moment(today);
                    // var total = null;
                    // if (dateToday.diff(converted_date, 'days') <= 30) {
                    //     total = dateToday.diff(converted_date, 'days') + "  days";
                    // }
                    // else if (dateToday.diff(converted_date, 'months') >= 1 && dateToday.diff(converted_date, 'months') < 12) {
                    //     total = dateToday.diff(converted_date, 'months') + "  months";
                    // }
                    // else {
                    //     total = dateToday.diff(converted_date, 'year') + "  year";
                    // }

                    return (
                        <div className="col-lg-8 review_box">

                            <div className="row ">
                                <div className="col-lg-12 stars show-large">
                                    <img className="social_icon_2" src={result.site.icon_url}
                                         title={result.site.name}/>&emsp;
                                    <span className="onl_review_title">{result.author}</span> &emsp;
                                    <StarRating style={starStyle} totalStars={5} rating={result.rating}
                                                disabled="disabled"/>
                                    <span className="days_ago"> {converted_date}</span>
                                </div>
                            </div>
                            <div className="row ">
                                <div className="col-lg-12 stars show-med">
                                    <img className="social_icon_2" src={result.site.icon_url}
                                         title={result.site.name}/>&emsp;
                                    <span className="onl_review_title">{result.author}</span> &emsp;
                                    <StarRating style={starStyle} totalStars={5} rating={result.rating}
                                                disabled="disabled"/>
                                    <span className="days_ago"> {converted_date}</span>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-xs-12 show-mobile">
                                    <img className="social_icon_2" src={result.site.icon_url} title={result.site.name}/>
                                    <span className="onl_review_title">{result.author}</span>
                                    <span className="days_ago"> {converted_date}</span>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-xs-12 show-mobile marg">
                                    <StarRating style={starStyle} totalStars={5} rating={result.rating}
                                                disabled="disabled"/>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-lg-12 ">
                                    <p className="onl_review">
                                        {result.body}
                                    </p>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-lg-12">
                                    <a href={result.response_url}><span className="respond"> Respond </span> </a>
                                </div>
                            </div>
                        </div>

                    )

                })
            }
            // return <div className="load_style"><img style={loadPosition} src="images/load_icon.gif"/></div>
            return <div className="col-lg-8 review_box">No Record found</div>
        };

        return (
            <div>
                <div className="bg_blu">
                    <div className="row">
                        <div className="col-lg-8 all_reviews">

                            <span className="all_review_head">All Reviews</span>
                            <input className="srch_button show-large"
                                   value={this.state.search}
                                   onChange={this.updateSearch.bind(this)}
                                   name="Search" placeholder="Search"/>
                            <input className="srch_button show-med" style={{'margin':'0'}}
                                   value={this.state.search}
                                   onChange={this.updateSearch.bind(this)}
                                   name="Search" placeholder="Search"/>
                        </div>
                        <div className="col-lg-4">
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xs-12 show-mobile">
                            <div className="mob_align_center">
                                <input className="srch_button_mob"
                                       value={this.state.search}
                                       onChange={this.updateSearch.bind(this)}
                                       name="Search" placeholder="Search"/>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        {reviewInfo()}
                    </div>
                </div>
            </div>
        );
    }
});

var Online = React.createClass({
    render: function () {
        var {reviewsJson, feedbackJson} = this.props;
        return (

            <div>

                <div className="row">
                    <div className="col-lg-12">
                        <div className="row show-large">
                            <div className="col-lg-12">
                                <span className="all_review_head"> Reviews </span>
                            </div>
                        </div>
                        <br/>
                        <div className="row">
                            <div className="col-lg-12 list_style ">
                                        <OnlineReview reviewsJson={reviewsJson}></OnlineReview>

                            </div>
                        </div>
                    </div>
                </div>

            </div>

        )
    }
});

module.exports = Online;