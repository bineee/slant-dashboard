    var React = require('react');
var LoginForm = require('LoginForm');
var LoginMessage = require('LoginMessage');
var Cookies = require('universal-cookie');
var {Route, Router, IndexRoute, hashHistory, browserHistory} = require('react-router');
var loginApi = require('loginApi');
var getCurrentUser = require('getCurrentUser');
var axios = require('axios');
const cookies = new Cookies();

var Login = React.createClass({

    getInitialState: function () {
        return {
            isLoading: false,
            errorMessage: undefined,
            access_token: '',
            userData: '',
        }
    },
    componentDidMount:function () {
        document.title = "Slant | Login";
        var loggedIn= cookies.get('token');
        var userData = cookies.get('user');
        const {history, location} = window;
         if (typeof (loggedIn)=== 'string' && typeof (userData)== 'string') {
            browserHistory.push("/analytics");
             // history.pushState(null, null, location.href);
             // window.onpopstate = event => {
             //     event.preventDefault();
             //     history.go(1);
             // };
        }
    },
    onLoginButtonClicked: function (email, password) {
        /*cookies.remove('token');
        cookies.remove('user');*/
        var that = this;
        this.setState({
            isLoading: true,
            userData: null,
            errorMessage: undefined,
            access_token: undefined,
        });
        loginApi.getLoggedIn(email, password).then(function (access_token) {
            that.setState({
                access_token: access_token,
                isLoading: false
            });
            cookies.set('token', access_token, {path: '/'});
            browserHistory.push("/analytics");
        }, function (e) {
            that.setState({
                isLoading: false,
                errorMessage: e.message
            });
        });
      /*  console.log("for current user info",cookies.get('token'));
        getCurrentUser.getCurrentUser(cookies.get('token')).then(function (res) {
            var result = JSON.stringify(res);
            cookies.set('user', result, {path: '/'});


        });*/
    },
    render: function () {
        var {isLoading, access_token, errorMessage} = this.state;

        function renderMessage() {
            if (isLoading) {
                return <h3 className="text-center">Verifying User...</h3>;
            } else if (access_token) {
               cookies.set("token", access_token);
                return;
            } else if (errorMessage) {
                return <LoginMessage checkError={errorMessage}/>
            }
        }

        return (
            <div className="bg_color">
                <LoginForm onLoginButtonClicked={this.onLoginButtonClicked}/>
                {renderMessage()}
            </div>
        )
    }
})

module.exports = Login;