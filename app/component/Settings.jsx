var React = require('react');
import {Tab, Tabs, TabList, TabPanel} from 'react-tabs';

var ReactDOM = require('react-dom');
var {Route, Router, IndexRoute, hashHistory, browserHistory, withRouter} = require('react-router');
var Cookies = require('universal-cookie');
const cookies = new Cookies();
const defaultURL = 'https://api.slantreviews.com/v2/locations/';
var axios = require('axios');
var loadPosition = {
    top: 'unset !important',
    margin: '5% 47%'
};

const sysPrimary = "Hi {{NAME}}, thanks for choosing XYZ Auto Repair! Do you mind taking a moment to leave us a review? This link makes it easy: {{LINK}}";
const sysFallBack = "Hi there! Thanks for choosing XYZ Auto Repair! Do you mind taking a moment to leave us a review? This link makes it easy: {{LINK}}";
var Link = React.createClass({
    getInitialState: function () {
        return {
            linkMutex: false,
            errorMessages: "",
            availableSites: {},
            linked_accounts: {},
            reviewAccounts: {},
            fields: {},
            url: "",
            site_name: ""
        };
    },
    handleLinkAccount: function () {
        this.setState({
            linkMutex: !this.state.linkMutex
        });
    },
    handleTextChange: function (field, e) {
        let fields = this.state.fields;
        fields[field] = e.target.value;
        this.setState({fields});
    },
    deleteLinkedAccount: function (result, index) {
        var array = this.props.reviewAccounts.linked_accounts;
        var location = this.props.locId;
        var site_id = result.site.id;
        var idx = array.indexOf(array[index]);
        var that = this;
        if (idx !== -1) {
            axios.delete(defaultURL + location + '/review_accounts/' + site_id, {headers: {Authorization: cookies.get('token')}}).then(function (res) {
                try {
                    if (res.status === 200) {
                        array.splice(idx, 1);
                        that.setState({reviewAccounts: res.data});
                        that.props.onErrorMessage("Successfully Deleted", 'success');
                        return true;
                    } else if (res.status >= 400) {
                        alert(res.data.message);
                        return false;
                    }
                }
                catch (e) {
                    throw e;
                }
            }, function (res) {
                var errorMessage = "Sorry some error occured";
                if (typeof (res.response.data) !== undefined) {
                    errorMessage = res.response.data.message;
                }
                that.props.onErrorMessage(errorMessage, 'error');
            });
        }
    },
    handleLinkNewAccount: function (e) {
        e.preventDefault();
        let fields = this.state.fields;
        var url = this.state.fields['url'];
        var site_id = this.state.fields['site_name'];
        if (!url || !site_id) {
            return;
        }
        this.state.fields['url'] = '';
        document.getElementById("site_name").value = '';
        var location = this.props.locId;
        var that = this;
        axios.defaults.headers.common['Authorization'] = cookies.get('token');
        axios.post(defaultURL + location + '/review_accounts', {
            url: url,
            site_id: site_id,
        }, {}).then(function (res) {
            try {

                if (res.status === 200 || res.status === 201) {
                    that.setState({reviewAccounts: res.data});
                    that.props.onErrorMessage("Successfully Added", 'success');
                    return true;
                } else if (res.status >= 400) {
                    return false;
                }
            }
            catch (e) {
                alert(res.data.message);
            }
        }, function (res) {
            var errorMessage = "Sorry some error occured";
            if (typeof (res.response.data) !== undefined) {
                errorMessage = res.response.data.message;
            }
            that.props.onErrorMessage(errorMessage, 'error');

        });
    },
    render: function () {
        var linkedAccounts = this.props.reviewAccounts.linked_accounts;
        var availableSites = this.props.reviewAccounts.available_sites;
        if (typeof(this.state.reviewAccounts.available_sites) !== "undefined") {
            linkedAccounts = this.state.reviewAccounts.linked_accounts;
            availableSites = this.state.reviewAccounts.available_sites;
            this.props.reviewAccounts.linked_accounts = this.state.reviewAccounts.linked_accounts;
            this.props.reviewAccounts.available_sites = this.state.reviewAccounts.available_sites;
        }

        var linkedAccountsArr = [];
        linkedAccounts.map((linkedAccounts, index) => {
            linkedAccountsArr.push(linkedAccounts.site.id);
        })
        var renderAvailableSites = () => {
            if (availableSites.length > 0) {
                return availableSites.map((avSites, index) => {
                    return (
                        <option key={avSites.id} value={avSites.id}
                                disabled={(linkedAccountsArr.indexOf(avSites.id) === -1) ? "" : "disabled"}>{avSites.name}</option>
                    )
                })
            }
        }
        var RenderReviewAccounts = () => {
            if (linkedAccounts.length > 0) {
                return linkedAccounts.map((linkedAccounts, index) => {
                    return (
                        <tr key={linkedAccounts.site.id}>
                            <td>
                                <div className="row">
                                    <div className="col-lg-4">
                                        {/*<div className="checkboxFour">
                                                <input type="checkbox" value="1" id="checkboxInput1" name=""/>
                                                <label htmlFor="checkboxInput1">
                                                </label>
                                            </div>*/}
                                        <img src={linkedAccounts.site.icon_url} className="social_icon"/>
                                    </div>
                                    <div className="col-lg-8">
                                        <span>{linkedAccounts.site.name}</span>
                                    </div>
                                </div>
                            </td>
                            <td className="team_email"><a target="_blank"
                                                          href={linkedAccounts.url}>{linkedAccounts.url}</a></td>
                            <td><img src="images/Cross.png" className="change_cursor"
                                     onClick={this.deleteLinkedAccount.bind(this, linkedAccounts, index)}/></td>
                        </tr>
                    )
                })
            }
            return <tr>
                <td> No record found. Add</td>
            </tr>
        };

        var RenderReviewAccountsMob = () => {
            if (linkedAccounts.length > 0) {
                return linkedAccounts.map((linkedAccounts, index) => {
                    return (
                        <div key={linkedAccounts.site.id}>

                            <div className="row">
                                <div className="col-xs-2">
                                    <img src={linkedAccounts.site.icon_url} className="social_icon"/>
                                </div>
                                <div className="col-xs-8">
                                    <span>{linkedAccounts.site.name}</span>
                                </div>
                                <div className="col-xs-2">
                                    <img src="images/Cross.png" className="change_cursor" style={{'float':'right'}} onClick={this.deleteLinkedAccount.bind(this, linkedAccounts, index)}/>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-xs-10" style={{'overflow':'hidden'}}>
                                    <a target="_blank" className="team_email" href={linkedAccounts.url}>{linkedAccounts.url}</a>
                                </div>
                            </div>
                            <hr/>
                        </div>


                    )
                })
            }
            return <div>
                <span> No record found. Add account now.</span>
            </div>
        };

        return (<div>
                <div className="row">
                    <div className="col-lg-8 all_reviews">

                        <div className="row">
                            <div className="col-lg-6 col-sm-6">
                                <span className="all_review_head hide-mobile">Link Your Accounts</span>
                            </div>
                            <div className="col-lg-6 col-sm-6">
                                <div className="form_filter hide-mobile">
                                    <button onClick={this.handleLinkAccount} className="btn btn_primary btn_add_team">
                                        Add Account
                                    </button>
                                    &emsp;
                                    <button className="btn btn_primary btn_filter" onClick={this.handleLinkNewAccount}>
                                        Save
                                    </button>
                                </div>
                            </div>
                        </div>


                        <span className="all_review_head show-mobile">Link Accounts</span>
                    </div>
                    <div className="col-lg-4">
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-12 show-mobile">
                        <div className="mob_align_center">
                            <button onClick={this.handleLinkAccount} className="btn btn_primary btn_add_team_m">Add
                                Account
                            </button>
                        </div>
                    </div>
                </div>
                <br className="show-mobile"/>
                <div className="row">
                    <div className="col-xs-12 show-mobile">
                        <div className="mob_align_center">
                            <button className="btn btn_primary btn_filter_m" onClick={this.handleLinkNewAccount}>Save
                            </button>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-8 table_box">

                        <div className=" table-responsive hide-mobile">

                            <table id="acc_table" className="table table-hover">
                                <thead>
                                <tr>
                                    <th>Site</th>
                                    <th>Page Link</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                {RenderReviewAccounts()}
                                <tr className={this.state.linkMutex ? "" : "invisi"}>
                                    <td>
                                        <div className="row">
                                            <div className="col-lg-12">
                                                <select id="site_name" name="site_name" ref="site_name"
                                                        className="edit_table small_edit"
                                                        onChange={this.handleTextChange.bind(this, "site_name")}>
                                                    <option key="" value="">Select Site</option>
                                                    {renderAvailableSites()}
                                                </select>
                                            </div>
                                        </div>

                                    </td>
                                    <td className="team_email">
                                        <input type="url" placeholder="www.AnyReviewSite.com"
                                               value={this.state.fields["url"]} ref="url"
                                               onChange={this.handleTextChange.bind(this, "url")}
                                               className="edit_table long_edit"
                                               maxLength="255"/>
                                    </td>
                                    <td></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                        <div className="show-mobile">
                            <span className="team_head">Site</span>
                            <span className="team_head" style={{'float':'right', 'margin':'0 0 0 10px'}}>Page Link</span>
                            <hr/>
                            {RenderReviewAccountsMob()}
                            <div className={this.state.linkMutex ? "" : "invisi"}>

                                <div className="row">
                                    <div className="col-xs-12">
                                        <select id="site_name" name="site_name" ref="site_name"
                                                className="edit_table small_edit"
                                                onChange={this.handleTextChange.bind(this, "site_name")}>
                                            <option key="" value="">Select Site</option>
                                            {renderAvailableSites()}
                                        </select>
                                    </div>
                                </div>
                                <br/>
                                <div className="row">
                                    <div className="col-xs-10 team_email" style={{'overflow':'hidden'}}>
                                        <input type="url" placeholder="www.AnyReviewSite.com"
                                               value={this.state.fields["url"]} ref="url"
                                               onChange={this.handleTextChange.bind(this, "url")}
                                               className="edit_table long_edit"
                                               maxLength="255"/>
                                    </div>
                                </div>
                                <br/>

                            </div>

                        </div>

                    </div>
                    <div className="col-lg-4">
                    </div>
                </div>
            </div>

        );
    }
});

var Team = React.createClass({

    getInitialState: function () {
        return {
            fields: {},
            role: "",
            first_name: "",
            errors: {},
            last_name: '',
            email: '',
            membersJson: {},
            teamMutex: false
        };
    },
    handleTextChange: function (field, e) {
        let fields = this.state.fields;
        fields[field] = e.target.value;
        this.setState({fields});
    },
    handleTeamAdd: function () {
        this.setState({
            teamMutex: !this.state.teamMutex
        });
    },
    validateField: function (field, e) {
        var charCode = e.keyCode || e.which;
        try {
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return true;
            }
            else if (charCode >= 48 && charCode <= 57) {
                e.preventDefault();
                return false;
            }
        }
        catch (err) {
            alert(err.Description);
        }
    },

    handleValidation(first_name, last_name, email, role) {
        let errors = {};
        let isValid = true;
        if (typeof (first_name) !== "undefined") {
            if (first_name.length > 40) {
                isValid = false;
                errors["first_name"] = "First Name Cannot be more than 40 characters";
            }
        }

        if (typeof (last_name) !== "undefined") {
            if (last_name.length > 40) {
                isValid = false;
                errors["first_name"] = "Last Name Cannot be more than 40 characters";
            }
        }

        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!re.test(email)) {
            isValid = false;
            errors["email"] = "Email is invalid";
        }
        this.setState({errors: errors});
        return isValid;


    },
    deleteMember: function (result, index) {
        var array = this.props.membersJson.members;
        var location = this.props.locId;
        var user_id = result.id;
        var idx = array.indexOf(array[index]);
        var that = this;
        if (idx !== -1) {
            axios.delete(defaultURL + location + '/members/' + user_id, {headers: {Authorization: cookies.get('token')}}).then(function (res) {
                try {
                    if (res.status === 200) {
                        array.splice(idx, 1);
                        that.setState({membersJson: res.data});
                        that.props.onErrorMessage("Successfully Deleted", 'success');
                        return true;
                    } else if (res.status >= 400) {
                        alert(res.data.message);
                        return false;
                    }
                }
                catch (e) {
                    throw e;
                }
            }, function (res) {
                var errorMessage = "Sorry some error occured";
                if (typeof (res.response.data) !== undefined) {
                    errorMessage = res.response.data.message;
                }
                that.props.onErrorMessage(errorMessage, 'error');
            });
        }
    },
    addTeamMember: function (e) {
        e.preventDefault();
        let fields = this.state.fields;
        var first_name = this.state.fields['first_name'];
        var last_name = this.state.fields['last_name'];
        var email = this.state.fields['email'];
        var role = this.state.fields['role'];
        if (!first_name || !last_name || !email || !role) {
            return;
        }
        if (!this.handleValidation(first_name, last_name, email, role)) {
            return
        }
        var location = this.props.locId;
        var members = this.props.membersJson;
        var that = this;
        axios.defaults.headers.common['Authorization'] = cookies.get('token');
        axios.post(defaultURL + location + '/members', {
            first_name: first_name,
            last_name: last_name,
            email: email,
            role: role
        }, {}).then(function (res) {
            try {

                if (res.status >= 200) {
                    // var m = members.members.push({
                    //     role: role,
                    //     last_name: last_name,
                    //     first_name: first_name,
                    //     email: email,
                    //     id:res.data.members.id
                    // });

                    that.setState({membersJson: res.data});
                    that.props.onErrorMessage("Successfully Added", 'success');
                    return true;
                } else if (res.status >= 400) {
                    alert(res.data.message);
                    return false;
                }
            }
            catch (e) {
                throw e;
            }
        }, function (res) {
            var errorMessage = "Sorry some error occured";
            if (typeof (res.response.data) !== undefined) {
                errorMessage = res.response.data.message;
            }
            that.props.onErrorMessage(errorMessage, 'error');
        });

        this.state.fields['first_name'] = '';
        this.state.fields['last_name'] = '';
        this.state.fields['email'] = '';
        this.state.fields['role'] = '';

    },
    render: function () {
        var members = this.props.membersJson;
        if (typeof(this.state.membersJson.members) !== "undefined") {
            members = this.state.membersJson;
            this.props.membersJson.members = this.state.membersJson.members;
        }
        var memberInfo = () => {
            if (jQuery.isEmptyObject(members) || typeof(members) === "undefined") {

                return <tr>
                    <td className="load_style" colSpan={6}><img style={loadPosition} src="images/load_icon.gif"/></td>
                </tr>
            }
            if (members.members.length > 0) {
                return members.members.map((result, index) => {
                    return (
                        <tr>
                            <td>{result.first_name} {result.last_name}</td>
                            <td>{result.email}</td>
                            <td>{result.role}</td>
                            <td><img onClick={this.deleteMember.bind(this, result, index)} className="change_cursor"
                                     src="images/Cross.png"/></td>
                        </tr>
                    )
                })
            }
            return <tr>
                <td className="load_style" colSpan={6}><img style={loadPosition} src="images/load_icon.gif"/></td>
            </tr>
        };

        var memberInfoMobile = () => {
            if (jQuery.isEmptyObject(members) || typeof(members) === "undefined") {

                return <div>
                    <div className="load_style" colSpan={6}><img style={loadPosition} src="images/load_icon.gif"/></div>
                </div>
            }
            if (members.members.length > 0) {
                return members.members.map((result, index) => {
                    return (

                        <div>

                            <span className="team_body">{result.first_name} {result.last_name}</span>
                            <img className="change_cursor" style={{'float':'right'}}
                                 src="images/Cross.png" onClick={this.deleteMember.bind(this, result, index)}/>
                            <br/>
                            <br/>
                            <span className="team_email">{result.email}</span>
                            <br/>
                            <br/>
                            <span className="team_body role" > {result.role}</span>
                            <hr/>

                        </div>

                    )
                })
            }
            return <div>
                <div className="load_style" colSpan={6}><img style={loadPosition} src="images/load_icon.gif"/></div>
            </div>
        };

        return (<div>

            <div className="row">
                <div className="col-lg-8 all_reviews">
                    <div className="row">
                        <div className="col-lg-6 col-sm-6">
                            <span className="all_review_head hide-mobile">Team Members</span>
                        </div>
                        <div className="col-lg-6 col-sm-6">
                            <div className="form_filter hide-mobile">
                                <button onClick={this.handleTeamAdd} className="btn btn_primary btn_add_team">Add
                                    Teammate
                                </button>
                                &emsp;
                                <button onClick={this.addTeamMember} className="btn btn_primary btn_filter">Save
                                </button>
                            </div>
                        </div>
                    </div>

                    <span className="all_review_head show-mobile">Team Management</span>

                </div>
                <div className="col-lg-4">
                </div>
            </div>
            <div className="row">
                <div className="col-xs-12 show-mobile">
                    <div className="mob_align_center">
                        <button onClick={this.handleTeamAdd} className="btn btn_primary btn_add_team_m">Add Teammate
                        </button>
                    </div>
                </div>
            </div>
            <br className="show-mobile"/>
            <div className="row">
                <div className="col-xs-12 show-mobile">
                    <div className="mob_align_center">
                        <button onClick={this.addTeamMember} className="btn btn_primary btn_filter_m">Save</button>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-lg-8 table_box">

                    <div className="table-responsive hide-mobile">
                        <table id="team_table" className="table table-hover">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            {memberInfo()}
                            <tr className={this.state.teamMutex ? "" : "invisi"}>
                                <td>
                                    <input type="text" placeholder="First name" value={this.state.fields["first_name"]}
                                           onChange={this.handleTextChange.bind(this, "first_name")}
                                           maxLength="20"
                                           onKeyPress={this.validateField.bind(this, "first_name")}
                                           className="edit_table small_edit"/>
                                    <span style={{
                                        color: "red",
                                        fontSize: "17px"
                                    }}>{this.state.errors["first_name"]}</span>
                                    <input type="text" placeholder="Last name" value={this.state.fields["last_name"]}
                                           onChange={this.handleTextChange.bind(this, "last_name")}
                                           className="edit_table small_edit"
                                           maxLength="20"
                                           onKeyPress={this.validateField.bind(this, "last_name")}/>
                                    <span style={{
                                        color: "red",
                                        fontSize: "17px"
                                    }}>{this.state.errors["last_name"]}</span>
                                </td>
                                <td className="team_email">
                                    <input type="email" placeholder="email" value={this.state.fields["email"]}
                                           onChange={this.handleTextChange.bind(this, "email")}
                                           maxlength="40"
                                           className="edit_table long_edit"/>
                                    <br/>
                                    <span style={{
                                        color: "red",
                                        fontSize: "17px"
                                    }}>{this.state.errors["email"]}</span>
                                </td>

                                <td>
                                    <select id="role" name="role" onChange={this.handleTextChange.bind(this, "role")}
                                            value={this.state.fields["role"]}>
                                        <option key="" value="">Select Role</option>
                                        <option key="admin" value="admin">Admin</option>
                                        <option key="member" value="member">Member</option>
                                    </select>
                                </td>
                            </tr>

                            </tbody>
                        </table>
                    </div>


                    <div style={{'color': '#4A4A4A'}} className="show-mobile">

                        <span className="team_head">Teammates</span>
                        <hr/>
                        {memberInfoMobile()}

                        <div className={this.state.teamMutex ? "" : "invisi"}>
                            <input type="text" placeholder="First name" value={this.state.fields["first_name"]}
                                   onChange={this.handleTextChange.bind(this, "first_name")}
                                   maxLength="20"
                                   onKeyPress={this.validateField.bind(this, "first_name")}
                                   className="edit_table small_edit"/>
                            <span style={{
                                color: "red",
                                fontSize: "17px"
                            }}>{this.state.errors["first_name"]}</span>
                            <input type="text" placeholder="Last name" value={this.state.fields["last_name"]}
                                   onChange={this.handleTextChange.bind(this, "last_name")}
                                   className="edit_table small_edit"
                                   maxLength="20"
                                   onKeyPress={this.validateField.bind(this, "last_name")}/>
                            <span style={{
                                color: "red",
                                fontSize: "17px"
                            }}>{this.state.errors["last_name"]}</span>
                            <br/>
                            <br/>
                            <input type="email" placeholder="email" value={this.state.fields["email"]}
                                   onChange={this.handleTextChange.bind(this, "email")}
                                   maxlength="40"
                                   className="edit_table team_email long_edit"/>

                            <span style={{
                                color: "red",
                                fontSize: "17px"
                            }}>{this.state.errors["email"]}</span>
                            <br/><br/>
                            <select id="role" name="role" onChange={this.handleTextChange.bind(this, "role")}
                                    value={this.state.fields["role"]}
                                    className="edit_table small_edit">
                                <option key="" value="">Select Role</option>
                                <option key="admin" value="admin">Admin</option>
                                <option key="member" value="member">Member</option>
                            </select>
                            <hr/>
                        </div>
                    </div>


                </div>
                <div className="col-lg-4">
                </div>
            </div>
        </div>
        );
    }
});

var Sms = React.createClass({

    getInitialState: function () {
        return {
            primary: '',
            fallback: '',
            fields: {},
            errors: {},
            editMutex: false,
        };
    },
    handleEditMessage: function () {
        this.setState({
            editMutex: !this.state.editMutex
        });
    },

    handlePrimaryMessageChange: function (event) {
        this.setState({primary: event.target.value});
    },
    handleFallbackMessageChange: function (event) {
        this.setState({fallback: event.target.value});
    },
    handleValidationMessage() {
        let fields = this.state.fields;
        if (typeof this.state.primary == "undefined")
            this.state.primary = $('#primary').val();
        if (typeof this.state.fallback == "undefined")
            this.state.fallback = $('#fallback').val();
        let errors = {};
        let formIsValid = true;
        var nameToMatch = '{{NAME}}';
        var linkToMatch = '{{LINK}}';
        if (!this.state.primary) {
            formIsValid = false;
            errors["primaryMsg"] = "Primary Message cannot be empty";
        }
        if (!this.state.fallback) {
            formIsValid = false;
            errors["fallbackMsg"] = "FallBack Message cannot be empty";
        }
        if (this.state.primary.indexOf(nameToMatch) == -1 || this.state.primary.indexOf(linkToMatch) == -1) {
            formIsValid = false;
            errors["primaryMsg"] = "Primary Message should contain " + nameToMatch + "and" + linkToMatch;
        }

        if (this.state.fallback.indexOf(linkToMatch) == -1) {
            formIsValid = false;
            errors["fallbackMsg"] = "FallBack Message should contain " + linkToMatch;
        }

        this.setState({errors: errors});
        return formIsValid;
    },


    modifyMessage: function (e) {
        e.preventDefault();
        let fields = this.state.fields;
        var primaryM = $('#primary').val();
        var fallbackM = $('#fallback').val();
        var primaryMsg = this.state.primary;
        var fallbackMsg = this.state.fallback;
        if (typeof this.state.primary == "undefined")
            var primaryMsg = $('#primary').val();
        if (typeof this.state.fallback == "undefined")
            var fallbackMsg = $('#fallback').val();
        if (!primaryMsg || !fallbackMsg) {
            return;
        }
        var location = this.props.locId;
        var message = this.props.messageJson;
        var that = this;

        if (this.handleValidationMessage()) {
            axios.defaults.headers.common['Authorization'] = cookies.get('token');
            axios.patch(defaultURL + location + '/review_invitation_body', {
                primary: primaryMsg,
                fallback: fallbackMsg,
            }, {}).then(function (res) {
                try {
                    if (res.status === 200) {

                        that.state.primary = primaryMsg;
                        that.state.fallback = fallbackMsg;
                        that.props.onErrorMessage("Successfully Updated", 'success');
                        that.state.fields = null;
                    } else if (res.status >= 400) {
                        alert(res.data.message);
                        return false;
                    }
                }
                catch (e) {
                    throw e;
                }
            }, function (res) {
                var errorMessage = "Sorry some error occured";
                if (typeof (res.response.data) !== undefined) {
                    errorMessage = res.response.data.message;
                }
                that.props.onErrorMessage(errorMessage, 'error');
            });
        }
        else {
            return false;
        }

        this.state.editMutex = false;
    },
    resetFallBackMessage: function (e) {
        var that = this;
        //that.state.editMutex = false;
        let fields = this.state.fields;
        that.setState({fallback: sysFallBack});
        $('#fallback').val(sysFallBack);
        $("#fallback").attr("disabled", "disabled");
    },
    resetPrimaryMessage: function (e) {
        var that = this;
        // that.state.editMutex = false;
        let fields = that.state.fields;
        that.setState({primary: sysPrimary});
        $('#primary').val(sysPrimary);
        $("#primary").attr("disabled", "disabled");

    },
    render: function () {
        if (this.state.primary || this.state.fallback) {
            this.props.messageJson.data.primary = this.state.primary;
            this.props.messageJson.data.fallback = this.state.fallback;
        }
        if (this.state.primary === '' && this.state.fallback === '') {
            this.state.primary = this.props.messageJson.data.primary;
            this.state.fallback = this.props.messageJson.data.fallback;
        }


        return (<div>
            <div className="row">
                <div className="col-lg-8 all_reviews">

                    <div className="row">
                        <div className="col-lg-6">
                            <span className="all_review_head show-large">Customize Primary SMS</span>
                        </div>
                        <div className="col-lg-6">
                            <div className="form_filter show-large">
                                <button onClick={this.handleEditMessage} disabled={this.state.editMutex}
                                        className="btn btn_primary btn_add_team">Edit
                                    Message
                                </button>
                                &emsp;
                                <button onClick={this.modifyMessage} className="btn btn_primary btn_filter"
                                        disabled={!this.state.editMutex}>Save
                                </button>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-6 col-sm-6">
                            <span className="all_review_head show-med">Customize Primary SMS</span>
                        </div>
                        <div className="col-lg-6 col-sm-6">
                            <div className="form_filter show-med">
                                <button onClick={this.handleEditMessage} disabled={this.state.editMutex}
                                        className="btn btn_primary btn_add_team">Edit
                                    Message
                                </button>
                                &emsp;
                                <button onClick={this.modifyMessage} className="btn btn_primary btn_filter"
                                        disabled={!this.state.editMutex}>Save
                                </button>
                            </div>
                        </div>
                    </div>
                    <span className="all_review_head show-mobile">Customize SMS</span>

                </div>
                <div className="col-lg-4">
                </div>
            </div>
            <div className="row">
                <div className="col-xs-12 show-mobile">
                    <div className="mob_align_center">
                        <button onClick={this.handleEditMessage} disabled={this.state.editMutex}
                                className="btn btn_primary btn_add_team_m">Edit
                            Message
                        </button>
                    </div>
                </div>
            </div>
            <br className="show-mobile"/>
            <div className="row">
                <div className="col-xs-12 show-mobile">
                    <div className="mob_align_center">
                        <button onClick={this.modifyMessage} className="btn btn_primary btn_filter_m"
                                disabled={!this.state.editMutex}>Save
                        </button>
                    </div>
                </div>
            </div>

            <div className="row">
                <div className="col-lg-8 table_box">
                    <div className="row">
                        <div className="col-lg-12">
                            <br/><br/>
                            <span className="msg_error">
                            {this.state.errors["primaryMsg"]}
                            </span>
                            <textarea className="edit_sms" id="primary"
                                      value={this.state.primary}
                                      disabled={!this.state.editMutex}
                                      onChange={this.handlePrimaryMessageChange}>
                           </textarea>
                        </div>


                    </div>

                    <div className="row">
                        <div className="col-lg-3">
                            <button onClick={this.resetPrimaryMessage} disabled={!this.state.editMutex}
                                    className="btn btn_primary btn_restore_default">
                                Reset to Default
                            </button>
                        </div>
                        <div className="col-lg-9">

                        </div>
                    </div>
                    <br/>
                    <div className="row">
                        <div className="col-lg-8">
                            <span className="reset_text">
                                This will reset your outgoing SMS message to our system default message. Your message will not be saved.
                            </span>
                        </div>
                        <div className="col-lg-4">

                        </div>
                    </div>
                    <br/>
                </div>
                <div className="col-lg-4">

                </div>

            </div>


            <div className="row">
                <div className="col-lg-8 all_reviews">
                    <span className="all_review_head">Customize Fallback SMS</span>
                </div>
                <div className="col-lg-4">
                </div>
            </div>
            <div className="row">
                <div className="col-lg-8 table_box">
                    <div className="row">
                        <div className="col-lg-12">
                            <br/><br/>
                            <span className="msg_error">
                            {this.state.errors["fallbackMsg"]}
                            </span>
                            <textarea className="edit_sms" id="fallback"
                                      value={this.state.fallback}
                                      disabled={!this.state.editMutex}
                                      onChange={this.handleFallbackMessageChange}>
                                                        </textarea>
                        </div>


                    </div>

                    <div className="row">
                        <div className="col-lg-3">
                            <button onClick={this.resetFallBackMessage} disabled={!this.state.editMutex}
                                    className="btn btn_primary btn_restore_default">
                                Reset to Default
                            </button>
                        </div>
                        <div className="col-lg-9">

                        </div>
                    </div>
                    <br/>
                    <div className="row">
                        <div className="col-lg-8">
                                                        <span className="reset_text">
                                                            This will reset your outgoing SMS message to our system default message. Your message will not be saved.
                                                        </span>
                        </div>
                        <div className="col-lg-4">

                        </div>
                    </div>
                    <br/>
                </div>
                <div className="col-lg-4">

                </div>

            </div>

        </div>)
    }
});

var Settings = React.createClass({
    getInitialState: function () {
        return {
            errorMessage: "",
            tabIndex :0,
        }
    },
    componentDidMount() {
        if (getClientRole(cookies.get("user"), cookies.get("selectedLocation")) === "member")
            browserHistory.push("/analytics")
        var settings = this.props.location.query;
        if(checkNested(settings,'q') && window.innerWidth <= 1200){
           var settingId = settings.q;
            if(settingId === 'link' && this.state.tabIndex!== 0){
                this.setState({tabIndex:0})
            }
            if(settingId === 'team' && this.state.tabIndex!== 1 ){
                console.log("hello");
                this.setState({tabIndex:1})
            }
            if(settingId === 'sms' && this.state.tabIndex!== 2){
                this.setState({tabIndex:2})
            }
    }},
    componentWillUpdate(nextProps, nextState){
        var settings = nextProps.location.query;
        if(checkNested(settings,'q') && window.innerWidth <= 1200){
            var settingId = settings.q;
            if(settingId === 'link' && this.state.tabIndex!== 0){
                this.setState({tabIndex:0})
            }
            if(settingId === 'team' && this.state.tabIndex!== 1){
                console.log("hello");
                this.setState({tabIndex:1})
            }
            if(settingId === 'sms' && this.state.tabIndex!== 2){
                this.setState({tabIndex:2})
            }
        }

    },
    handleErrorMessage: function (errorMessages, type) {
        this.props.handleErrorMessageInDashBoard(errorMessages, type);
    },
    render: function () {

        var {selectedLocation, membersJson, messageJson, locId, reviewAccounts} = this.props;
        if (membersJson === '' || typeof(membersJson) === undefined || messageJson === '' ||
            typeof(messageJson) === undefined || !checkNested(membersJson, 'members') || !checkNested(messageJson, 'data') ||
            !checkNested(reviewAccounts, 'available_sites') || !checkNested(reviewAccounts, 'linked_accounts')) {
            return (
                <div className="load_style"><img src="images/load_icon.gif"/></div>
            )
        }
        return (

            <div>

                <div className="row">
                    <div className="col-lg-12">
                        <div className="row">
                            <div className="col-lg-12">
                                <span className="all_review_head show-large"> Settings </span>
                            </div>
                        </div>
                        <br/>
                        <div className="row">
                            <div className="col-lg-12 list_style">
                                <Tabs selectedIndex={this.state.tabIndex} onSelect={tabIndex => this.setState({ tabIndex })}>
                                    <div className="show-large">
                                        <TabList>
                                            <Tab>Link Accounts</Tab>
                                            <Tab>Team Management</Tab>
                                            <Tab>Customize SMS</Tab>
                                        </TabList>
                                    </div>

                                    <TabPanel className="bg_blu_m">
                                        <Link reviewAccounts={reviewAccounts} locId={locId}
                                              onErrorMessage={this.handleErrorMessage}/>
                                    </TabPanel>

                                    <TabPanel className="bg_blu_m"><Team membersJson={membersJson}
                                                                       locId={locId}
                                                                       onErrorMessage={this.handleErrorMessage}/>
                                    </TabPanel>

                                    <TabPanel className="bg_blu_m"><Sms messageJson={messageJson}
                                                                      locId={locId}
                                                                      onErrorMessage={this.handleErrorMessage}/>


                                    </TabPanel>

                                </Tabs>

                            </div>
                        </div>
                    </div>
                </div>


            </div>

        )
    }
});

module.exports = Settings;