var React = require('react');
import {Tab, Tabs, TabList, TabPanel} from 'react-tabs';

var ReactDOM = require('react-dom');
var {Route, Router, IndexRoute, hashHistory, browserHistory, withRouter} = require('react-router');
var Cookies = require('universal-cookie');
const cookies = new Cookies();
const defaultURL = 'https://api.slantreviews.com/v2/locations/';
var axios = require('axios');
var loadPosition = {
    top: 'unset !important',
    margin: '5% 47%'
};

const sysPrimary = "Hi {{NAME}}, thanks for choosing XYZ Auto Repair! Do you mind taking a moment to leave us a review? This link makes it easy: {{LINK}}";
const sysFallBack = "Hi there! Thanks for choosing XYZ Auto Repair! Do you mind taking a moment to leave us a review? This link makes it easy: {{LINK}}";

var Team = React.createClass({

    getInitialState: function () {
        return {
            fields: {},
            role: "",
            first_name: "",
            errors: {},
            last_name: '',
            email: '',
            membersJson: {},
            teamMutex: false
        };
    },
    handleTextChange: function (field, e) {
        let fields = this.state.fields;
        fields[field] = e.target.value;
        this.setState({fields});
    },
    handleTeamAdd: function () {
        this.setState({
            teamMutex: !this.state.teamMutex
        });
    },
    validateField: function (field, e) {
        var charCode = e.keyCode || e.which;
        try {
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return true;
            }
            else if (charCode >= 48 && charCode <= 57) {
                e.preventDefault();
                return false;
            }
        }
        catch (err) {
            alert(err.Description);
        }
    },

    handleValidation(first_name, last_name, email, role) {
        let errors = {};
        let isValid = true;
        if (typeof (first_name) !== "undefined") {
            if (first_name.length > 40) {
                isValid = false;
                errors["first_name"] = "First Name Cannot be more than 40 characters";
            }
        }

        if (typeof (last_name) !== "undefined") {
            if (last_name.length > 40) {
                isValid = false;
                errors["first_name"] = "Last Name Cannot be more than 40 characters";
            }
        }

        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!re.test(email)) {
            isValid = false;
            errors["email"] = "Email is invalid";
        }
        this.setState({errors: errors});
        return isValid;


    },
    deleteMember: function (result, index) {
        var array = this.props.membersJson.members;
        var location = this.props.locId;
        var user_id = result.id;
        var idx = array.indexOf(array[index]);
        var that = this;
        if (idx !== -1) {
            axios.delete(defaultURL + location + '/members/' + user_id, {headers: {Authorization: cookies.get('token')}}).then(function (res) {
                try {
                    if (res.status === 200) {
                        array.splice(idx, 1);
                        that.setState({membersJson: res.data});
                        that.props.onErrorMessage("Successfully Deleted", 'success');
                        return true;
                    } else if (res.status >= 400) {
                        alert(res.data.message);
                        return false;
                    }
                }
                catch (e) {
                    throw e;
                }
            }, function (res) {
                var errorMessage = "Sorry some error occured";
                if (typeof (res.response.data) !== undefined) {
                    errorMessage = res.response.data.message;
                }
                that.props.onErrorMessage(errorMessage, 'error');
            });
        }
    },
    addTeamMember: function (e) {
        e.preventDefault();
        let fields = this.state.fields;
        var first_name = this.state.fields['first_name'];
        var last_name = this.state.fields['last_name'];
        var email = this.state.fields['email'];
        var role = this.state.fields['role'];
        if (!first_name || !last_name || !email || !role) {
            return;
        }
        if (!this.handleValidation(first_name, last_name, email, role)) {
            return
        }
        var location = this.props.locId;
        var members = this.props.membersJson;
        var that = this;
        axios.defaults.headers.common['Authorization'] = cookies.get('token');
        axios.post(defaultURL + location + '/members', {
            first_name: first_name,
            last_name: last_name,
            email: email,
            role: role
        }, {}).then(function (res) {
            try {

                if (res.status >= 200) {
                    // var m = members.members.push({
                    //     role: role,
                    //     last_name: last_name,
                    //     first_name: first_name,
                    //     email: email,
                    //     id:res.data.members.id
                    // });

                    that.setState({membersJson: res.data});
                    that.props.onErrorMessage("Successfully Added", 'success');
                    return true;
                } else if (res.status >= 400) {
                    alert(res.data.message);
                    return false;
                }
            }
            catch (e) {
                throw e;
            }
        }, function (res) {
            var errorMessage = "Sorry some error occured";
            if (typeof (res.response.data) !== undefined) {
                errorMessage = res.response.data.message;
            }
            that.props.onErrorMessage(errorMessage, 'error');
        });

        this.state.fields['first_name'] = '';
        this.state.fields['last_name'] = '';
        this.state.fields['email'] = '';
        this.state.fields['role'] = '';

    },
    render: function () {
        var members = this.props.membersJson;
        if (typeof(this.state.membersJson.members) !== "undefined") {
            members = this.state.membersJson;
            // this.state.membersJson = {};
        }
        var memberInfo = () => {
            if (jQuery.isEmptyObject(members) || typeof(members) === "undefined") {

                return <tr>
                    <td className="load_style" colSpan={6}><img style={loadPosition} src="images/load_icon.gif"/></td>
                </tr>
            }
            if (members.members.length > 0) {
                return members.members.map((result, index) => {
                    return (
                        <tr>
                            <td>{result.first_name} {result.last_name}</td>
                            <td>{result.email}</td>
                            <td>{result.role}</td>
                            <td><img onClick={this.deleteMember.bind(this, result, index)} className="change_cursor"
                                     src="images/Cross.png"/></td>
                        </tr>
                    )
                })
            }
            return <tr>
                <td className="load_style" colSpan={6}><img style={loadPosition} src="images/load_icon.gif"/></td>
            </tr>
        };

        var memberInfoMobile = () => {
            if (jQuery.isEmptyObject(members) || typeof(members) === "undefined") {

                return <div>
                    <div className="load_style" colSpan={6}><img style={loadPosition} src="images/load_icon.gif"/></div>
                </div>
            }
            if (members.members.length > 0) {
                return members.members.map((result, index) => {
                    return (

                            <div>

                                <span className="team_body">{result.first_name} {result.last_name}</span>
                                <img className="change_cursor" style={{'float':'right'}}
                                     src="images/Cross.png" onClick={this.deleteMember.bind(this, result, index)}/>
                                <br/>
                                <br/>
                                <span className="team_email">{result.email}</span>
                                <br/>
                                <br/>
                                <span className="team_body role" > {result.role}</span>
                                <hr/>

                            </div>

                    )
                })
            }
            return <div>
                <div className="load_style" colSpan={6}><img style={loadPosition} src="images/load_icon.gif"/></div>
            </div>
        };

        return (<div>

            <div className="row">
                <div className="col-lg-8 all_reviews">
                    <div className="row">
                        <div className="col-lg-6 col-sm-6">
                            <span className="all_review_head show-med">Team Members</span>
                        </div>
                        <div className="col-lg-6 col-sm-6">
                            <div className="form_filter show-med">
                                <button onClick={this.handleTeamAdd} className="btn btn_primary btn_add_team">Add
                                    Teammate
                                </button>
                                &emsp;
                                <button onClick={this.addTeamMember} className="btn btn_primary btn_filter">Save
                                </button>
                            </div>
                        </div>
                    </div>

                    <span className="all_review_head show-mobile">Team Management</span>

                </div>
                <div className="col-lg-4">
                </div>
            </div>
            <div className="row">
                <div className="col-xs-12 show-mobile">
                    <div className="mob_align_center">
                        <button onClick={this.handleTeamAdd} className="btn btn_primary btn_add_team_m">Add Teammate
                        </button>
                    </div>
                </div>
            </div>
            <br className="show-mobile"/>
            <div className="row">
                <div className="col-xs-12 show-mobile">
                    <div className="mob_align_center">
                        <button onClick={this.addTeamMember} className="btn btn_primary btn_filter_m">Save</button>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-lg-8 table_box">

                    <div className="table-responsive show-med">
                        <table id="team_table" className="table table-hover">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            {memberInfo()}
                            <tr className={this.state.teamMutex ? "" : "invisi"}>
                                <td>
                                    <input type="text" placeholder="First name" value={this.state.fields["first_name"]}
                                           onChange={this.handleTextChange.bind(this, "first_name")}
                                           maxLength="20"
                                           onKeyPress={this.validateField.bind(this, "first_name")}
                                           className="edit_table small_edit"/>
                                    <span style={{
                                        color: "red",
                                        fontSize: "17px"
                                    }}>{this.state.errors["first_name"]}</span>
                                    <input type="text" placeholder="Last name" value={this.state.fields["last_name"]}
                                           onChange={this.handleTextChange.bind(this, "last_name")}
                                           className="edit_table small_edit"
                                           maxLength="20"
                                           onKeyPress={this.validateField.bind(this, "last_name")}/>
                                    <span style={{
                                        color: "red",
                                        fontSize: "17px"
                                    }}>{this.state.errors["last_name"]}</span>
                                </td>
                                <td className="team_email">
                                    <input type="email" placeholder="email" value={this.state.fields["email"]}
                                           onChange={this.handleTextChange.bind(this, "email")}
                                           maxlength="40"
                                           className="edit_table long_edit"/>
                                    <br/>
                                    <span style={{
                                        color: "red",
                                        fontSize: "17px"
                                    }}>{this.state.errors["email"]}</span>
                                </td>

                                <td>
                                    <select id="role" name="role" onChange={this.handleTextChange.bind(this, "role")}
                                            value={this.state.fields["role"]}>
                                        <option key="" value="">Select Role</option>
                                        <option key="admin" value="admin">Admin</option>
                                        <option key="member" value="member">Member</option>
                                    </select>
                                </td>
                            </tr>

                            </tbody>
                        </table>
                    </div>


                    <div style={{'color': '#4A4A4A'}} className="show-mobile">

                        <span className="team_head">Teammates</span>
                        <hr/>
                        {memberInfoMobile()}

                        <div className={this.state.teamMutex ? "" : "invisi"}>
                            <input type="text" placeholder="First name" value={this.state.fields["first_name"]}
                                   onChange={this.handleTextChange.bind(this, "first_name")}
                                   maxLength="20"
                                   onKeyPress={this.validateField.bind(this, "first_name")}
                                   className="edit_table small_edit"/>
                            <span style={{
                                color: "red",
                                fontSize: "17px"
                            }}>{this.state.errors["first_name"]}</span>
                            <input type="text" placeholder="Last name" value={this.state.fields["last_name"]}
                                   onChange={this.handleTextChange.bind(this, "last_name")}
                                   className="edit_table small_edit"
                                   maxLength="20"
                                   onKeyPress={this.validateField.bind(this, "last_name")}/>
                            <span style={{
                                color: "red",
                                fontSize: "17px"
                            }}>{this.state.errors["last_name"]}</span>
                            <br/>
                            <br/>
                            <input type="email" placeholder="email" value={this.state.fields["email"]}
                                   onChange={this.handleTextChange.bind(this, "email")}
                                   maxlength="40"
                                   className="edit_table team_email long_edit"/>

                            <span style={{
                                color: "red",
                                fontSize: "17px"
                            }}>{this.state.errors["email"]}</span>
                            <br/><br/>
                            <select id="role" name="role" onChange={this.handleTextChange.bind(this, "role")}
                                    value={this.state.fields["role"]}
                                    className="edit_table small_edit">
                                <option key="" value="">Select Role</option>
                                <option key="admin" value="admin">Admin</option>
                                <option key="member" value="member">Member</option>
                            </select>
                            <hr/>
                        </div>
                    </div>

                </div>
                <div className="col-lg-4">
                </div>
            </div>
        </div>);
    }
});


var TeamM = React.createClass({
    getInitialState: function () {
        return {
            errorMessage: ""
        }
    },
    componentDidMount() {
        if (getClientRole(cookies.get("user"), cookies.get("selectedLocation")) === "member")
            browserHistory.push("/analytics")
    },
    handleErrorMessage: function (errorMessages, type) {
        this.props.handleErrorMessageInDashBoard(errorMessages, type);
    },
    render: function () {

        var {selectedLocation, membersJson, messageJson, locId, reviewAccounts} = this.props;
        if (membersJson === '' || typeof(membersJson) === undefined || messageJson === '' ||
            typeof(messageJson) === undefined || !checkNested(membersJson, 'members') || !checkNested(messageJson, 'data') ||
            !checkNested(reviewAccounts, 'available_sites') || !checkNested(reviewAccounts, 'linked_accounts')) {
            return (
                <div className="load_style"><img src="images/load_icon.gif"/></div>
            )
        }
        return (

            <div>

                <div className="row">
                    <div className="col-lg-12">
                        <div className="row">
                            <div className="col-lg-12">
                                <span className="all_review_head show-large"> Settings </span>
                            </div>
                        </div>
                        <br/>
                        <div className="row">
                            <div className="col-lg-12 bg_blu_m">
                                <Team membersJson={membersJson}
                                      locId={locId}
                                      onErrorMessage={this.handleErrorMessage}/>

                            </div>
                        </div>
                    </div>
                </div>


            </div>

        )
    }
});

module.exports = TeamM;