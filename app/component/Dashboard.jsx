var React = require('react');
var Analytics = require('Analytics');
var NavSideBar = require('NavSideBar');
var TopNav = require('TopNav');
var Cookies = require('universal-cookie');
var Review = require('Review');
var Customer = require('Customer');
var Settings = require('Settings');
var Private = require('Private');
var locationDropDownApi = require('locationDropDownApi');
var LocationStats = require('LocationStats');
var axios = require('axios');
import Notifications, {notify} from 'react-notify-toast';

const cookies = new Cookies();

var rowStyle = {
    height: '100vh',
    position: 'relative'
};

var Dashboard = React.createClass({

    getInitialState: function () {
        return {
            locationArray: [],
            errorMessage: '',
            selectedLocation: '',
            statsJson: '',
            reviewsJson: '',
            customerCount: 0,
            customerJson:'',
            reviewAccounts:'',
            privateFeedBack:''
        }
    },

    componentDidMount() {
        var that = this;
        document.title = "Slant | Dashboard";
        var currentUserURL = "https://api.slantreviews.com/v2/users/me";
        axios.get(currentUserURL, {headers: {Authorization: cookies.get('token')}}).then(function (res) {
            try {
                if (res.status < 400) {
                    cookies.set("user",res.data);
                    locationDropDownApi.getLocationByUser(res.data.id).then(function (jsonString) {
                        var defaultSelectedLocation = '';
                        if(!cookies.get('selectedLocation')) {
                            defaultSelectedLocation = jsonString.locations[0].id;
                            cookies.set('selectedLocation', jsonString.locations[0].id);
                        }else{
                            defaultSelectedLocation = cookies.get('selectedLocation');
                        }
                        that.setState({
                            locationArray: jsonString,
                            selectedLocation: defaultSelectedLocation
                        })
                    }, function (e) {
                        that.setState({
                            errorMessage: e.message
                        });
                    })
                } else if (res.status >= 400) {
                    alert(res.data.message);
                    return false;
                }
            }
            catch (e) {
                throw e;
            }
        }, function (res) {
        });

    },
    handleErrorMessageInDashBoard: function (errorMessage, type) {
        this.setState({errorMessage:errorMessage});
        if(type === 'error') {
            let myColor = {background: '#84130d', text: "#FFFFFF"};
            notify.show(errorMessage, "custom", 5000, myColor);
        }else{
            let myColor = {background: '#38D996', text: "#FFFFFF"};
            notify.show(errorMessage, "custom", 5000, myColor);
        }

    },
    handleSentInvitation: function(c){
      var that = this;
        that.setState({customerJson: c});
        that.setState({customerCount: c.length});
    },
    handleLocationChange: function (location) {
        var that =this;
        that.setState({
            statsJson: '',
            reviewsJson: '',
            customerCount: 0,
            customerJson:'',
            reviewAccounts:'',
            privateFeedBack:'',
            locationArray:{}
        });

        this.getLocationStatistics(location);
        //var that = this;
        var user_id = cookies.get('user').id;
        locationDropDownApi.getLocationByUser(user_id).then(function (jsonString) {
            that.setState({
                locationArray: jsonString,
                selectedLocation: location
            });
        }, function (e) {
            that.setState({
                errorMessage: e.message
            });
        })
    },

    updateCustomerCount:function (c) {
        this.setState({customerCount:c})
    },
    getLocationStatistics: function (location_id) {
        var that = this;
        var client_role = getClientRole(cookies.get('user'), location_id);
        const defaultURL = 'https://api.slantreviews.com/v2/locations/';


        axios.all([
            axios.get(defaultURL + location_id + '/stats', {headers: {Authorization: cookies.get('token')}}),
            axios.get(defaultURL + location_id + '/reviews', {headers: {Authorization: cookies.get('token')}}),
            axios.get(defaultURL + location_id + '/review_invitations', {headers: {Authorization: cookies.get('token')}}),
            axios.get(defaultURL + location_id + '/feedback', {headers: {Authorization: cookies.get('token')}}),
        ])
            .then(axios.spread(function (stats, reviews, customers,privateFeedback) {
                var statsJson = stats.data;
                var reviewsJson = reviews.data;
                var customerJson = customers.data;
                var privateFeedback = privateFeedback.data.feedback;
                var locId = location_id;
                var customerCount = customers.data.review_invitations.length;
                that.setState({statsJson: statsJson});
                that.setState({reviewsJson: reviewsJson});
                that.setState({customerJson: customerJson});
                that.setState({locId: location_id});
                that.setState({customerCount:customerCount});
                that.setState({feedbackJson:privateFeedback});
            }))
            .catch(error => console.log(error));
        if(client_role === "admin") {


            axios.all([
                axios.get(defaultURL + location_id + '/members', {headers: {Authorization: cookies.get('token')}}),
                axios.get(defaultURL + location_id + '/review_invitation_body', {headers: {Authorization: cookies.get('token')}}),
                axios.get(defaultURL + location_id + '/review_accounts', {headers: {Authorization: cookies.get('token')}}),
            ])
                .then(axios.spread(function (members, message, reviewAccounts) {
                    var membersJson = members.data;
                    var messageJson = message.data;
                    var reviewAccounts = reviewAccounts.data;
                    that.setState({membersJson: membersJson});
                    that.setState({messageJson: message});
                    that.setState({reviewAccounts: reviewAccounts});
                }))
                .catch(error => console.log(error));
        }
    },
    render: function () {
        var renderLoader = () => {
            return <div className="load_style"><img src="images/load_icon.gif"/></div>
        };
        var {locationArray, statsJson, selectedLocation, reviewsJson, customerJson, locId, membersJson, messageJson,customerCount, reviewAccounts,feedbackJson} = this.state;
        if (locationArray.locations === undefined) {
            return <div className="row">{renderLoader()}</div>
        }
        if (selectedLocation !== '' && statsJson === '') {
            this.getLocationStatistics(selectedLocation);
        }
        return (
            <div className="row">
                <div style={{"display": (this.state.errorMessages)!==""?"":"none"}}>
                    <Notifications/>
                </div>
                <div className="col-lg-2 side_ko">
                    <NavSideBar reviewsJson={reviewsJson} locId={locId} customerCount={customerCount} selectedLocation={selectedLocation}
                                customerJson={customerJson} onInvitationSent = {this.handleSentInvitation} updateCustomerCount = {this.updateCustomerCount}
                                handleErrorMessageInDashBoard={this.handleErrorMessageInDashBoard}/>
                </div>
                <div className="col-lg-10">
                    <div className="row">
                        <div className="col-lg-12">
                            <TopNav locationArray={locationArray} onLocationChangeGraph={this.handleLocationChange} handleErrorMessageInDashBoard={this.handleErrorMessageInDashBoard}/>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-12">
                            {React.cloneElement(this.props.children, {
                                customerCount: customerCount,
                                statsJson: statsJson,
                                reviewsJson: reviewsJson,
                                selectedLocation: selectedLocation,
                                customerJson: customerJson,
                                membersJson: membersJson,
                                messageJson: messageJson,
                                reviewAccounts: reviewAccounts,
                                feedbackJson:feedbackJson,
                                locId:locId,
                                updateCustomerCount:this.updateCustomerCount,
                                handleErrorMessageInDashBoard: this.handleErrorMessageInDashBoard

                            })}

                        </div>
                    </div>
                </div>
            </div>
        );
    }
});

module.exports = Dashboard;