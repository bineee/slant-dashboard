var React = require('react');
import {Tab, Tabs, TabList, TabPanel} from 'react-tabs';

var ReactDOM = require('react-dom');
var {Route, Router, IndexRoute, hashHistory, browserHistory, withRouter} = require('react-router');
var Cookies = require('universal-cookie');
const cookies = new Cookies();
const defaultURL = 'https://api.slantreviews.com/v2/locations/';
var axios = require('axios');
var loadPosition = {
    top: 'unset !important',
    margin: '5% 47%'
};

const sysPrimary = "Hi {{NAME}}, thanks for choosing XYZ Auto Repair! Do you mind taking a moment to leave us a review? This link makes it easy: {{LINK}}";
const sysFallBack = "Hi there! Thanks for choosing XYZ Auto Repair! Do you mind taking a moment to leave us a review? This link makes it easy: {{LINK}}";


var Sms = React.createClass({

    getInitialState: function () {
        return {
            primary: '',
            fallback: '',
            fields: {},
            errors: {},
            editMutex: false,
        };
    },
    handleEditMessage: function () {
        this.setState({
            editMutex: !this.state.editMutex
        });
    },

    handlePrimaryMessageChange: function (event) {
        this.setState({primary: event.target.value});
    },
    handleFallbackMessageChange: function (event) {
        this.setState({fallback: event.target.value});
    },
    handleValidationMessage() {
        let fields = this.state.fields;
        if (typeof this.state.primary == "undefined")
            this.state.primary = $('#primary').val();
        if (typeof this.state.fallback == "undefined")
            this.state.fallback = $('#fallback').val();
        let errors = {};
        let formIsValid = true;
        var nameToMatch = '{{NAME}}';
        var linkToMatch = '{{LINK}}';
        if (!this.state.primary) {
            formIsValid = false;
            errors["primaryMsg"] = "Primary Message cannot be empty";
        }
        if (!this.state.fallback) {
            formIsValid = false;
            errors["fallbackMsg"] = "FallBack Message cannot be empty";
        }
        if (this.state.primary.indexOf(nameToMatch) == -1 || this.state.primary.indexOf(linkToMatch) == -1) {
            formIsValid = false;
            errors["primaryMsg"] = "Primary Message should contain " + nameToMatch + "and" + linkToMatch;
        }

        if (this.state.fallback.indexOf(linkToMatch) == -1) {
            formIsValid = false;
            errors["fallbackMsg"] = "FallBack Message should contain " + linkToMatch;
        }

        this.setState({errors: errors});
        return formIsValid;
    },


    modifyMessage: function (e) {
        e.preventDefault();
        let fields = this.state.fields;
        var primaryM = $('#primary').val();
        var fallbackM = $('#fallback').val();
        var primaryMsg = this.state.primary;
        var fallbackMsg = this.state.fallback;
        if (typeof this.state.primary == "undefined")
            var primaryMsg = $('#primary').val();
        if (typeof this.state.fallback == "undefined")
            var fallbackMsg = $('#fallback').val();
        if (!primaryMsg || !fallbackMsg) {
            return;
        }
        var location = this.props.locId;
        var message = this.props.messageJson;
        var that = this;

        if (this.handleValidationMessage()) {
            axios.defaults.headers.common['Authorization'] = cookies.get('token');
            axios.patch(defaultURL + location + '/review_invitation_body', {
                primary: primaryMsg,
                fallback: fallbackMsg,
            }, {}).then(function (res) {
                try {
                    if (res.status === 200) {

                        that.state.primary = primaryMsg;
                        that.state.fallback = fallbackMsg;
                        that.props.onErrorMessage("Successfully Updated", 'success');
                        that.state.fields = null;
                    } else if (res.status >= 400) {
                        alert(res.data.message);
                        return false;
                    }
                }
                catch (e) {
                    throw e;
                }
            }, function (res) {
                var errorMessage = "Sorry some error occured";
                if (typeof (res.response.data) !== undefined) {
                    errorMessage = res.response.data.message;
                }
                that.props.onErrorMessage(errorMessage, 'error');
            });
        }
        else {
            return false;
        }

        this.state.editMutex = false;
    },
    resetFallBackMessage: function (e) {
        var that = this;
        //that.state.editMutex = false;
        let fields = this.state.fields;
        that.setState({fallback: sysFallBack});
        $('#fallback').val(sysFallBack);
        $("#fallback").attr("disabled", "disabled");
    },
    resetPrimaryMessage: function (e) {
        var that = this;
        // that.state.editMutex = false;
        let fields = that.state.fields;
        that.setState({primary: sysPrimary});
        $('#primary').val(sysPrimary);
        $("#primary").attr("disabled", "disabled");

    },
    render: function () {
        if (this.state.primary || this.state.fallback) {
            this.props.messageJson.data.primary = this.state.primary;
            this.props.messageJson.data.fallback = this.state.fallback;
        }
        if (this.state.primary === '' && this.state.fallback === '') {
            this.state.primary = this.props.messageJson.data.primary;
            this.state.fallback = this.props.messageJson.data.fallback;
        }


        return (
            <div>
            <div className="row">
                <div className="col-lg-8 all_reviews">

                    <div className="row">
                        <div className="col-lg-6">
                            <span className="all_review_head show-large">Customize Primary SMS</span>
                        </div>
                        <div className="col-lg-6">
                            <div className="form_filter show-large">
                                <button onClick={this.handleEditMessage} disabled={this.state.editMutex}
                                        className="btn btn_primary btn_add_team">Edit
                                    Message
                                </button>
                                &emsp;
                                <button onClick={this.modifyMessage} className="btn btn_primary btn_filter"
                                        disabled={!this.state.editMutex}>Save
                                </button>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-6 col-sm-6">
                            <span className="all_review_head show-med">Customize Primary SMS</span>
                        </div>
                        <div className="col-lg-6 col-sm-6">
                            <div className="form_filter show-med">
                                <button onClick={this.handleEditMessage} disabled={this.state.editMutex}
                                        className="btn btn_primary btn_add_team">Edit
                                    Message
                                </button>
                                &emsp;
                                <button onClick={this.modifyMessage} className="btn btn_primary btn_filter"
                                        disabled={!this.state.editMutex}>Save
                                </button>
                            </div>
                        </div>
                    </div>

                    <span className="all_review_head show-mobile">Customize SMS</span>
                </div>
                <div className="col-lg-4">
                </div>
            </div>
            <div className="row">
                <div className="col-xs-12 show-mobile">
                    <div className="mob_align_center">
                        <button onClick={this.handleEditMessage} disabled={this.state.editMutex}
                                className="btn btn_primary btn_add_team_m">Edit
                            Message
                        </button>
                    </div>
                </div>
            </div>
            <br className="show-mobile"/>
            <div className="row">
                <div className="col-xs-12 show-mobile">
                    <div className="mob_align_center">
                        <button onClick={this.modifyMessage} className="btn btn_primary btn_filter_m"
                                disabled={!this.state.editMutex}>Save
                        </button>
                    </div>
                </div>
            </div>

            <div className="row">
                <div className="col-lg-8 table_box">
                    <div className="row">
                        <div className="col-lg-12">
                            <br/><br/>
                            <span className="msg_error">
                            {this.state.errors["primaryMsg"]}
                            </span>
                            <textarea className="edit_sms" id="primary"
                                      value={this.state.primary}
                                      disabled={!this.state.editMutex}
                                      onChange={this.handlePrimaryMessageChange}>
                           </textarea>
                        </div>


                    </div>

                    <div className="row">
                        <div className="col-lg-3">
                            <button onClick={this.resetPrimaryMessage} disabled={!this.state.editMutex}
                                    className="btn btn_primary btn_restore_default">
                                Reset to Default
                            </button>
                        </div>
                        <div className="col-lg-9">

                        </div>
                    </div>
                    <br/>
                    <div className="row">
                        <div className="col-lg-8">
                            <span className="reset_text">
                                This will reset your outgoing SMS message to our system default message. Your message will not be saved.
                            </span>
                        </div>
                        <div className="col-lg-4">

                        </div>
                    </div>
                    <br/>
                </div>
                <div className="col-lg-4">

                </div>

            </div>


            <div className="row">
                <div className="col-lg-8 all_reviews">
                    <span className="all_review_head">Customize Fallback SMS</span>
                </div>
                <div className="col-lg-4">
                </div>
            </div>
            <div className="row">
                <div className="col-lg-8 table_box">
                    <div className="row">
                        <div className="col-lg-12">
                            <br/><br/>
                            <span className="msg_error">
                            {this.state.errors["fallbackMsg"]}
                            </span>
                            <textarea className="edit_sms" id="fallback"
                                      value={this.state.fallback}
                                      disabled={!this.state.editMutex}
                                      onChange={this.handleFallbackMessageChange}>
                                                        </textarea>
                        </div>


                    </div>

                    <div className="row">
                        <div className="col-lg-3">
                            <button onClick={this.resetFallBackMessage} disabled={!this.state.editMutex}
                                    className="btn btn_primary btn_restore_default">
                                Reset to Default
                            </button>
                        </div>
                        <div className="col-lg-9">

                        </div>
                    </div>
                    <br/>
                    <div className="row">
                        <div className="col-lg-8">
                                                        <span className="reset_text">
                                                            This will reset your outgoing SMS message to our system default message. Your message will not be saved.
                                                        </span>
                        </div>
                        <div className="col-lg-4">

                        </div>
                    </div>
                    <br/>
                </div>
                <div className="col-lg-4">

                </div>

            </div>

        </div>)
    }
});

var CustomSms = React.createClass({
    getInitialState: function () {
        return {
            errorMessage: ""
        }
    },
    componentDidMount() {
        if (getClientRole(cookies.get("user"), cookies.get("selectedLocation")) === "member")
            browserHistory.push("/analytics")
    },
    handleErrorMessage: function (errorMessages, type) {
        this.props.handleErrorMessageInDashBoard(errorMessages, type);
    },
    render: function () {

        var {selectedLocation, membersJson, messageJson, locId, reviewAccounts} = this.props;
        if (membersJson === '' || typeof(membersJson) === undefined || messageJson === '' ||
            typeof(messageJson) === undefined || !checkNested(membersJson, 'members') || !checkNested(messageJson, 'data') ||
            !checkNested(reviewAccounts, 'available_sites') || !checkNested(reviewAccounts, 'linked_accounts')) {
            return (
                <div className="load_style"><img src="images/load_icon.gif"/></div>
            )
        }
        return (

            <div>

                <div className="row">
                    <div className="col-lg-12">
                        <div className="row">
                            <div className="col-lg-12">
                                <span className="all_review_head show-large"> Settings </span>
                            </div>
                        </div>
                        <br/>
                        <div className="row">
                                <div className="col-lg-12 bg_blu_m">
                               <Sms messageJson={messageJson}
                                    locId={locId}
                                    onErrorMessage={this.handleErrorMessage}/>

                            </div>
                        </div>
                    </div>
                </div>


            </div>

        )
    }
});

module.exports = CustomSms;