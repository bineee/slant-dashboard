var React = require('react');
var Modal = require('react-modal');
var {Link, IndexLink} = require('react-router');
var axios = require('axios');


var Forgot = React.createClass({
    getInitialState: function () {
        return this.state = {
            fields: {},
            errors: {},
            isActive:false,
            forgotEmailError:'',
        }
    },

    handleChange(field, e) {
        let fields = this.state.fields;
        fields[field] = e.target.value;
        this.setState({fields});
    },
    handleValidation() {
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;

        //Email
        if (!fields["email"]) {
            formIsValid = false;
            errors["email"] = "Username cannot be empty";
        }

        if (typeof fields["email"] !== "undefined") {
            let lastAtPos = fields["email"].lastIndexOf('@');
            let lastDotPos = fields["email"].lastIndexOf('.');

            if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["email"].indexOf('@@') == -1 && lastDotPos > 2 && (fields["email"].length - lastDotPos) > 2)) {
                formIsValid = false;
                errors["email"] = "Username is not valid";
            }
        }
        //password
        if (!fields["password"]) {
            formIsValid = false;
            errors["password"] = "Password cannot be empty";
        }

        if (typeof fields["password"] !== "undefined") {
            if (fields["password"].length < 6) {
                formIsValid = false;
                errors["password"] = "Password Cannot be less than 6 character";
            }

        }
        this.setState({errors: errors});
        return formIsValid;
    },

    onForgotSubmit(e){
        e.preventDefault();
        var forgotEmail = this.refs.forgot_email.value;
        if(forgotEmail === ''){
            this.setState({forgotEmailError: "Email field cannot be empty"});
            return
        }

        let lastAtPos = forgotEmail.lastIndexOf('@');
        let lastDotPos = forgotEmail.lastIndexOf('.');

        if (!(lastAtPos < lastDotPos && lastAtPos > 0 && forgotEmail.indexOf('@@') == -1 && lastDotPos > 2 &&
                (forgotEmail.length - lastDotPos) > 2)) {
            this.setState({forgotEmailError: "Invalid Email"});
            return
        }
        var that = this;
        var defaultUrl  = "https://api.slantreviews.com/v2/auth/reset_link"
        //axios.defaults.headers.common['Authorization'] = cookies.get('token');
        axios.post(defaultUrl, {
            email: forgotEmail,
            domain: window.location.host,
            /*domain: "app.slantreviews.com",*/
        }, {}).then(function (res) {
            try {
                if (res.status === 200 || res.status === 201) {
                    that.setState({forgotEmailError: res.data.message});
                } else if (res.status >= 400) {
                    alert(res.data.message);
                    return false;
                }
            }
            catch (e) {
                throw e;
            }
        }, function (res) {
            var errorMessage = "Sorry some error occured";
            if (typeof (res.response.data) !== "undefined") {
                that.setState({forgotEmailError: res.response.data.message});
            }
        });
    },
    render: function () {
        return (
            <div className="container bg_color">
                <div className="row">
                    <div className="col-md-4">

                    </div>
                    <div className="col-md-4 col-xs-12 box">
                        <div className="row">
                            <div className="col-md-2 col-xs-1">
                            </div>
                            <div className="col-md-8 col-xs-10 box_inside">
                                <form className="reviews_await" onSubmit={this.onForgotSubmit}>
                                    <h3>Reset your password</h3>
                                    We'll send you a link
                                    <br/>
                                    <br/>
                                    <input type="email" placeholder="Email" className="log_pass" ref="forgot_email"
                                           required autoFocus
                                    />
                                    <button className="btn btn_primary btn_signin">
                                        <img src="images/airplane.png"/>&#8195;  Send Now
                                    </button>
                                    <br/>
                                    <p>{this.state.forgotEmailError}</p>

                                </form>
                            </div>
                            <div className="col-md-2 col-xs-1">
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4">

                    </div>
                </div>
            </div>
        );
    }
});

module.exports = Forgot;