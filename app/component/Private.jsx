var React = require('react');
import {Tab, Tabs, TabList, TabPanel} from 'react-tabs';
import StarRating from 'react-star-rating';


var moment = require('moment');
var starStyle = {
    width: '20px !important',
};


var loadPosition = {
    top: 'unset !important',
    margin: '5% 47%'
}


var PrivateFeedback = React.createClass({
    getInitialState: function () {
        return this.state = {
            searchPrivate: '',
        }
    },
    componentWillReceiveProps: function(nextProps){
        this.setState({searchPrivate:''})
    },
    updateSearchPrivate(event) {
        var searchValue = event.target.value;
        searchValue = searchValue.toString();
        searchValue = searchValue.toLowerCase();
        this.setState({searchPrivate: searchValue});
    },
    render: function () {
        var feedbackJson = this.props.feedbackJson;
        console.log(feedbackJson);
        let filteredPrivateFeedback = [];
        if (feedbackJson != '') {
            filteredPrivateFeedback = feedbackJson.filter(
                (detail) => {
                    if (typeof(detail.client_name) !== "undefined" && typeof(detail.feedback_body) !== "undefined") {
                        return (
                            detail.client_name.toLowerCase().indexOf(this.state.searchPrivate) >= 0
                            || detail.feedback_body.toLowerCase().indexOf(this.state.searchPrivate) >= 0
                        )
                    }
                }
            );
        }
        var feedbackInfo = () => {
            if (typeof(feedbackJson) === "undefined") {
                return <div className="load_style"><img style={loadPosition} src="images/load_icon.gif"/></div>
            }
            if (jQuery.isEmptyObject(feedbackJson) && feedbackJson.length === 0) {
                return <div className="col-lg-8 review_box">No Record found</div>
            }

            if (filteredPrivateFeedback.length > 0) {
                return filteredPrivateFeedback.map((result, index) => {
                    var dateTimeString = moment.unix(result.feedback_date).format("DD MMMM ,YYYY");
                    var feeback_date = dateTimeString.toString("MMMM");

                    return (
                        <div className="col-lg-8 review_box">

                            <div className="row">
                                <div className="col-lg-12 stars show-large">
                                    <span className="onl_review_title">{result.client_name}</span> &emsp;
                                    <img src="images/SmallSadie.png"/>
                                    <span className="days_ago">{feeback_date}</span>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-lg-12 stars show-med">
                                    <span className="onl_review_title">{result.client_name}</span> &emsp;
                                    <img src="images/SmallSadie.png"/>
                                    <span className="days_ago">{feeback_date}</span>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-xs-12 show-mobile">
                                    <span className="onl_review_title">{result.client_name}</span>
                                    <span className="days_ago">{feeback_date}</span>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-xs-12 show-mobile marg">
                                    <img src="images/SmallSadie.png"/>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-lg-12 ">
                                    <p className="onl_review">{result.feedback_body}

                                    </p>
                                </div>
                            </div>
                        </div>

                    )

                })
            }
            return  <div className="col-lg-8 review_box">No Record found</div>
        };
        return (
            <div>
                <div className="bg_blu_m">
                    <div className="row">
                        <div className="col-lg-8 all_reviews">
                            <span className="all_review_head">All Private Feedback</span>
                            <input className="srch_button show-large"
                                   value={this.state.searchPrivate}
                                   onChange={this.updateSearchPrivate.bind(this)}
                                   name="Search" placeholder="Search"/>
                            <input className="srch_button show-med" style={{'margin':'0'}}
                                   value={this.state.searchPrivate}
                                   onChange={this.updateSearchPrivate.bind(this)}
                                   name="Search" placeholder="Search"/>

                        </div>
                        <div className="col-lg-4">
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xs-12 show-mobile">
                            <div className="mob_align_center">
                                <input className="srch_button_mob"
                                       value={this.state.searchPrivate}
                                       onChange={this.updateSearchPrivate.bind(this)}
                                       name="Search" placeholder="Search"/>
                            </div>
                        </div>
                    </div>
                    <div className="row">

                        {feedbackInfo()}


                    </div>
                </div>
            </div>
        );
    }
});



var Private = React.createClass({
    render: function () {
        var {reviewsJson, feedbackJson} = this.props; var feedbackJson = this.props.feedbackJson;
        console.log(this.props.feedbackJson);
        return (

            <div>

                <div className="row">
                    <div className="col-lg-12">
                        <div className="row show-large">
                            <div className="col-lg-12">
                                <span className="all_review_head"> Reviews </span>
                            </div>
                        </div>
                        <br/>
                        <div className="row">

                            <div className="col-lg-12 list_style ">

                                <PrivateFeedback feedbackJson={feedbackJson}/>

                            </div>
                        </div>
                    </div>
                </div>

            </div>

        )
    }
});

module.exports = Private;