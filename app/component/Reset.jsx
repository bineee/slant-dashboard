var React = require('react');
var Cookies = require('universal-cookie');
var {Route, Router, IndexRoute, hashHistory, browserHistory} = require('react-router');
import Notifications, {notify} from 'react-notify-toast';
var axios = require('axios');
const cookies = new Cookies();

var Reset = React.createClass({

    getInitialState: function () {
        return {
            isLoading: false,
            resetCode: '',
            resetEmail: '',
            fields: {},
            errors: {},
            serverErrors:'',
            errorMessage:''
        }
    },
    componentDidMount:function () {
        document.title = "Slant | Reset Password";
        if(typeof(this.props.location.query.email) === 'undefined' || typeof(this.props.location.query.unique_code) === 'undefined'){
            browserHistory.push("/");
        }

        var emailId = this.props.location.query.email;
        var unique_code = this.props.location.query.unique_code;
        var that = this;
        that.setState({resetEmail:emailId, resetCode:unique_code});
    },
    handleErrorMessage: function (errorMessage, type) {
        this.setState({errorMessage:errorMessage});
        if(type === 'error') {
            let myColor = {background: '#84130d', text: "#FFFFFF"};
            notify.show(errorMessage, "custom", 5000, myColor);
        }else{
            let myColor = {background: '#38D996', text: "#FFFFFF"};
            notify.show(errorMessage, "custom", 5000, myColor);
        }

    },
    handleChange(field, e) {
        let fields = this.state.fields;
        fields[field] = e.target.value;
        this.setState({fields});
    },
    handleValidation() {
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;

        /**Client Side Validation for Phone Number*/
        if (!fields["password"]) {
            formIsValid = false;
            errors["password"] = "Password cannot be empty";
        }

        if (typeof fields["password"] !== "undefined") {
            if (fields["password"].length < 6) {
                formIsValid = false;
                errors["password"] = "Password cannot be less than 6 characters";
            }
        }

        if (!fields["confPassword"]) {
            formIsValid = false;
            errors["confPassword"] = "Password cannot be empty";
        }

       if(fields["password"] !== fields["confPassword"]){
            formIsValid = false;
            errors["confPassword"] = "Password didn't match";
       }
       this.setState({errors:errors});

        return formIsValid;
    },

    onFormSubmit: function (e) {
        e.preventDefault();
        let fields = this.state.fields;
        var password = fields["password"];
        var defaultURL = "https://api.slantreviews.com/v2/auth/reset/"+this.state.resetCode;

        if (this.handleValidation()) {
            var that = this;
            axios.post(defaultURL, {
                new_password: password,
            }, {}).then(function (res) {
                try {
                    if (res.status === 201 || res.status == 200) {
                        cookies.remove('selectedLocation');
                        cookies.remove('user');
                        cookies.set('token',res.data.access_token);
                        that.handleErrorMessage("Password Reset Successfully","success");
                        window.location.href = "/analytics";
                        return true;
                    } else {
                        alert(res.data.message);
                        fields["password"] = '';
                        fields["confPassword"] = '';
                        return false;
                    }
                }
                catch (e) {
                    throw e;
                }

            },function (res) {
                var errorMessage = "Sorry some error occured";
                if(typeof (res.response.data) !== undefined) {
                    errorMessage = res.response.data.message;
                }
                that.setState({serverErrors: errorMessage});
            });
            //this.props.onInvitationSent(c);

        } else {
            return false;
        }
    },
    render: function () {
        var {resetEmail, resetCode, serverErrors, errors, fields} = this.state;
        return (
            <div className="container bg_color">
                <div style={{"display": (this.state.errorMessages)!==""?"":"none"}}>
                    <Notifications/>
                </div>
                <div className="row">
                    <div className="col-sm-4">

                    </div>
                    <div className="col-sm-4 box">
                        <div className="row">
                            <div className="col-sm-2 col-md-1">
                            </div>
                            <div className="col-sm-8 col-md-10 box_inside">
                                <form className="reviews_await" onSubmit={this.onFormSubmit}>
                                    Set your password
                                    <br/>
                                    <br/>

                                    <span>
                                        {resetEmail}
                                    </span>
                                    <input type="password" placeholder="Password" id="reset_password" className="log_pass"
                                           onChange={this.handleChange.bind(this, "password")}
                                           value={fields["password"]} autoFocus required/>
                                    <span className="msg_error">{this.state.errors["password"]}</span>
                                    <input type="password" placeholder="Confirm Password" id="reset_confirm" className="log_pass"
                                           onChange={this.handleChange.bind(this, "confPassword")}
                                           value={fields["confPassword"]} required/>
                                    <span className="msg_error">{this.state.errors["confPassword"]}</span>
                                    <button className="btn btn-primary btn_signin">
                                        <img src="images/airplane.png"/>&#8195; Save and Sign In
                                    </button>
                                    <span className="msg_error">{serverErrors}</span>

                                </form>
                                <br/>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        )}
})

module.exports = Reset;
