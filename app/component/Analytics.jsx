import {NavSideBar} from "NavSideBar";

var React = require('react');
var {Link, IndexLink} = require('react-router');
import CircularProgressbar from 'react-circular-progressbar';
var Rating = require('react-rating');

var starStyle = {
    width: '20px !important',
};

var Analytics = React.createClass({
    componentDidMount:function () {
        document.title = "Slant | Analytics"
    },
    render: function () {
        var {statsJson, selectedLocation, reviewsJson} = this.props;
        var invitationJson = '';
        var total_reviews = 0;
        var reviews_by_site = [];
        var reviews_by_rating = '';
        var average_rating = 0;
        var max_rating = 0;
        var rating_over_last_30_days = 0;
        var total_increase_30_days = 0;

        var rating = [];

        // var reverseObj = function(object) {
        //     console.log(arr);
        //     var keys = Object.keys(object);
        //     for (var i = keys.length-1; i >= 0; i--) {
        //         var k = keys[i],
        //             v = object[k];
        //         console.log(k+":",v);
        //         array1[k] = v;
        //         array1.reverse();
        //     }
        //
        //     // var keys = new Array();
        //     //
        //     // for (var k in reviews_by_rating) {
        //     //     keys.unshift(k);
        //     // }
        //     // var index = keys.length;
        //     // for (var c = keys.length, n = 0; n < c; n++) {
        //     //     console.log(reviews_by_rating[index]);
        //     //     list[index]=reviews_by_rating[keys[n]];
        //     //     index --;
        //     // }
        //
        //     // var NewObj = {}, keysArr = Object.keys(object);
        //     // for (var i = keysArr.length-1; i >= 0; i--) {
        //     //     NewObj[keysArr[i]] = (object[keysArr[i]]);
        //     // }
        //     // Object.keys(reviews_by_rating).forEach(function (key) {
        //     //     for (var i = key.length-1; i >= 0; i--) {
        //     //         list[key] = reviews_by_rating[key];
        //     //
        //     //     }
        //         // do something with obj[key]
        //        // list.push(NewObj[key]);
        //    // });
        //     console.log(array1);
        //     return array1;
        // }
        if (statsJson !== '') {
            invitationJson = statsJson.invitations;
            total_reviews = statsJson.reviews.total_count;
            reviews_by_site = statsJson.reviews.by_site;
             total_increase_30_days = statsJson.reviews.total_increase_30_days;

            reviews_by_rating = statsJson.reviews.by_rating;
            reviews_by_site.sort(function (obj1, obj2) {
                return obj1.order - obj2.order;

            });
            average_rating = statsJson.reviews.average_rating;
            rating_over_last_30_days = statsJson.reviews.average_rating_delta_30_days;
        }
        var rating = Object.keys(reviews_by_rating);
        rating.sort(function (a,b) {
            return b-a;
        })
        max_rating = Math.max.apply(null, rating);
        const ReviewsByRates = () =>
            <div style={{'padding':'0 0 0 20px'}}>
                {
                    rating.map(([rate, index]) =>
                        <div className="col-lg-12 col-xs-12" key={rate} style={{padding:'0 0 5px 0'}}>
                            <img src="images/SmallSilverReview.png" style={{display: 'inline-block', float: 'left'}}/>
                            &nbsp;
                            <span style={{display: 'inline-block', float: 'left', 'margin':'-2px 7px -2px 7px'}}>{rate==1?'\u00a0'+rate:rate}</span>
                            &nbsp;
                            <div className="progress " style={{display: 'inline-block', float: 'left', width: (reviews_by_rating[rate] > 0 ? (reviews_by_rating[rate] / total_reviews * 100 + '%') : '3px' )}}>
                                <div className= {'pb'+rate+' '+ 'progress-bar'} role="progressbar"
                                     aria-valuemin="0" aria-valuemax={max_rating}
                                     style={{width: '100%'}}>
                                </div>
                            </div>
                            &nbsp;
                            <span style={{display: 'inline-block', float: 'left'}}>{reviews_by_rating[rate]}</span>

                        </div>

                    )
                }
            </div>

        var renderSites = () => {
            if (reviews_by_site.length > 0) {
                return reviews_by_site.map((site, index) => {
                        return (
                            <span key={site.url}>
                          <a href={site.url}> <img className="social_icon" src={site.site.icon_url} title={site.site.name}/></a>
                          <a href={site.response_url}>
                              <figcaption>{site.total_reviews}</figcaption>
                          </a>
                      </span>
                        )
                })
            }
            return <div>Loading...</div>
        };

        var renderTopSitesReview = () => {

            if (reviews_by_site.length > 0) {
                var site = reviews_by_site[0];
                return (
                    <div>
                        <div className="col-lg-6 col-sm-6">
                            <a href={site.url}><img className="social_icon" src={site.site.icon_url}/></a>
                            <span className="big">{site.average_rating}</span>
                            <img src="images/smallreview.png" className="siddha"/>
                            <br/>

                            <span>{site.site.name} is your top rated review site.</span>
                        </div>
                        <br className="show-mobile"/>
                        <div className="col-lg-6 col-sm-6">
                            <img src="images/smileycolor.png"/><span className="big">+{total_increase_30_days}</span>
                            <br/>

                            <span>positive reviews gained this month.</span>
                        </div>

                    </div>
                )
            }
        }

        var reviewLifethruRate = () => {
            var lifePercent = isNaN(invitationJson.opened / invitationJson.sent * 100) ? 0 : round(invitationJson.opened / invitationJson.sent * 100,2);
            if (lifePercent > 60) {
                return ( <div className="col-lg-12">

                    <span className="gjob">Great Job!</span>
                    <br/>
                    <span>Your CTR is higher than average.</span>
                </div>)
            }
            else {
                return ( <div className="col-lg-12">
                        <span className="gjob">Keep Going</span>
                        <br/>
                        <span>Your CTR is below average.</span>
                    </div>
                )
            }
        };
        if(statsJson === '' || typeof(statsJson.reviews)=== undefined ||
            typeof(statsJson.invitations) === undefined || !checkNested(statsJson, 'reviews', 'by_rating')
        || !checkNested(statsJson, 'reviews', 'by_site')){
            return (
                <div className="load_style"><img src="images/load_icon.gif"/></div>
            );
        }
        return (
            <div className="bg_blu_m">
            <div className="a_pad">

                <div className="row">
                    <div className="col-lg-5">
                        <div className="analytics_box1">
                            <div className="row">
                                <div className="col-lg-12">
                                    <span className="review_head"> Reviews </span>
                                </div>
                            </div>
                            <hr/>
                            <div className="row milako">
                                <div className="col-lg-12">
                                    <span className="revno">{total_reviews}</span>
                                </div>
                            </div>
                            <div className="row milako">
                                {renderTopSitesReview()}
                            </div>

                            <hr width="90%"/>
                            <div className="row milako">
                                <div className="col-lg-12">
                                    <span>Sites with review</span>
                                    <br/>
                                    <div className="custom_site_review">
                                        {renderSites()}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="col-lg-5 ">
                        <div className="analytics_box2">
                            <div className="row">
                                <div className="col-lg-12">
                                    <span className="review_head"> Lifetime Avg Rating  </span>
                                </div>
                            </div>
                            <hr/>
                            <div className="row milako">
                                <div className="col-lg-12">
                                    <span className="rat">{Math.round(average_rating * 10) / 10}</span>
                                    <div className="star_align">
                                        <Rating start={0} stop={5}
                                                empty={<img src="images/Reviews.png" className="icon"/>}
                                                placeholder={<img src="images/GoldReview.png" className="icon"/>}
                                                full={<img src="images/GoldReview.png" className="icon"/>}
                                                readonly="false" fractions="1"
                                                placeholderRate={Math.round(average_rating * 10) / 10}/>
                                    </div>
                                    {/*&#8195;*/}
                                </div>
                            </div>
                            <div className="row milako">
                                <div className="col-lg-12">


                                    <img src={(rating_over_last_30_days > 0) ? "images/arrowup.png" : ((rating_over_last_30_days < 0)? "images/arrow.png" : "images/arrowno.png")}/>&emsp;
                                    <span className="big">{rating_over_last_30_days}</span>
                                    <img src="images/SmallGoldReview.png" className="siddha"/>&emsp;
                                    <span>Your rating over the last <br/>&#8195;&#8195;&#8195;&#8195;&#8195;&#8195;&emsp;&nbsp;30 days.</span>
                                </div>
                            </div>
                            <br/>
                            <div className="row milako">
                                <ReviewsByRates/>
                            </div>
                        </div>
                    </div>

                    <div className="col-lg-2">
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-10">
                        <div className="row">
                            <div className="col-lg-4">
                                <div className="analytics_box3">
                                    <div className="row">
                                        <div className="col-lg-12">
                                            <span className="review_head"> Sentiment  </span>
                                        </div>
                                    </div>
                                    <hr/>
                                    <div className="row ">
                                        <div className="col-lg-12 senti_body">
                                            <span className="reco_no">{invitationJson.recommended}</span>
                                            &emsp;<img src="images/BigSmiley.png"/>&emsp;
                                            <span
                                                className="reco_per">{isNaN(invitationJson.recommended / invitationJson.sent * 100) ? 0 : round(invitationJson.recommended / invitationJson.sent * 100,0)}%</span>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-lg-12 senti_body">
                                            <span>Would Recommend</span>
                                        </div>
                                    </div>
                                    <hr width="70%"/>
                                    <div className="row">
                                        <div className="col-lg-12 senti_body">
                                            <span className="reco_no">{invitationJson.not_recommended}</span>
                                            &emsp;<img src="images/BigSadie.png"/>&emsp;
                                            <span
                                                className="noreco_per">{isNaN(invitationJson.not_recommended / invitationJson.sent * 100) ? 0 : round(invitationJson.not_recommended / invitationJson.sent * 100,0)}%</span>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-lg-12 senti_body">
                                            <span>Would Not </span>
                                            <br/>
                                            <span>Recommend</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="col-lg-4">
                                <div className="analytics_box4">
                                    <div className="row">
                                        <div className="col-lg-12">
                                            <span className="review_head"> Link Click-thru Rate  </span>
                                        </div>
                                    </div>
                                    <hr/>
                                    <div className="row">
                                        <div className="col-lg-12 senti_body">
                                            <div className="row">

                                            </div>
                                            <CircularProgressbar
                                                percentage={isNaN(invitationJson.opened / invitationJson.sent * 100) ? 0 : round(invitationJson.opened / invitationJson.sent * 100,0)}/>
                                            <hr width="50%"/>
                                            <div className="row">
                                                {reviewLifethruRate()}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="col-lg-4">

                            </div>

                        </div>
                    </div>
                    <div className="col-lg-2">

                    </div>
                </div>


            </div>
            </div>
        );
    }
});

module.exports = Analytics;