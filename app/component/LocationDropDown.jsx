var React = require('react');
var {Link, IndexLink} = require('react-router');
var locationDropDownApi = require('locationDropDownApi');
var {Route, Router, IndexRoute, hashHistory, browserHistory, withRouter} = require('react-router');
var Cookies = require('universal-cookie');
var cookies = new Cookies();

const ddStyle = {
    content: {
        background: '#FFFFFF',
        width: '100%',
    }
};

var LocationDropDown = React.createClass({
    getInitialState: function () {
        return {
            selectedLocation: ''
        }
    },

    componentDidMount() {
        if (!cookies.get('selectedLocation')) {
            console.log("no cookies fount");
            this.setState({selectedLocation: this.props.locationArray.locations[0].id})
            cookies.set("selectedLocation", this.props.locationArray.locations[0].id)
        } else {
            this.setState({selectedLocation: cookies.get("selectedLocation")})
        }
    },
    onLocationChange: function (e) {
        cookies.set("selectedLocation", e.target.value);
        if (getClientRole(cookies.get("user"), cookies.get("selectedLocation")) === "member" && window.location.href.toString().indexOf("settings") !== -1)
            browserHistory.push("/analytics")

        this.setState({selectedLocation: e.target.value});
        this.props.onLocationChangeStat(e.target.value);
    },
    render: function () {
        var {locationArray, errorMessage, selectedLocation} = this.props;
        if (typeof(cookies.get("selectedLocation")) !== undefined) {
            selectedLocation = cookies.get("selectedLocation")
        }
        var renderOptions = () => {
            return locationArray.locations.map((loca, index) => {
                return (
                    <option key={loca.id} value={loca.id}>{loca.name}</option>
                )
            })
        }

        if (locationArray.locations === undefined) {
            return <div>Loading...</div>
        }
        return (
            <div className="dropdown loc_dd">

                <select id="location" name="location" value={this.state.selectedLocation}
                        className="col-lg-4 selectpicker" onChange={this.onLocationChange}>
                    <optgroup label="Select Location">
                        {renderOptions()}
                    </optgroup>

                </select>
            </div>

        );
    }
});

module.exports = LocationDropDown;
