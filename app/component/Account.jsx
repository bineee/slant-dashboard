var React = require('react');
import {Tab, Tabs, TabList, TabPanel} from 'react-tabs';

var ReactDOM = require('react-dom');
var {Route, Router, IndexRoute, hashHistory, browserHistory, withRouter} = require('react-router');
var Cookies = require('universal-cookie');
const cookies = new Cookies();
const defaultURL = 'https://api.slantreviews.com/v2/locations/';
var axios = require('axios');
var loadPosition = {
    top: 'unset !important',
    margin: '5% 47%'
};

const sysPrimary = "Hi {{NAME}}, thanks for choosing XYZ Auto Repair! Do you mind taking a moment to leave us a review? This link makes it easy: {{LINK}}";
const sysFallBack = "Hi there! Thanks for choosing XYZ Auto Repair! Do you mind taking a moment to leave us a review? This link makes it easy: {{LINK}}";

var Link = React.createClass({
    getInitialState: function () {
        return {
            linkMutex: false,
            errorMessages: "",
            availableSites: {},
            linked_accounts: {},
            reviewAccounts: {},
            fields: {},
            url: "",
            site_name: ""
        };
    },
    handleLinkAccount: function () {
        this.setState({
            linkMutex: !this.state.linkMutex
        });
    },
    handleTextChange: function (field, e) {
        let fields = this.state.fields;
        fields[field] = e.target.value;
        this.setState({fields});
    },
    deleteLinkedAccount: function (result, index) {
        var array = this.props.reviewAccounts.linked_accounts;
        var location = this.props.locId;
        var site_id = result.site.id;
        var idx = array.indexOf(array[index]);
        var that = this;
        if (idx !== -1) {
            axios.delete(defaultURL + location + '/review_accounts/' + site_id, {headers: {Authorization: cookies.get('token')}}).then(function (res) {
                try {
                    if (res.status === 200) {
                        array.splice(idx, 1);
                        that.setState({reviewAccounts: res.data});
                        that.props.onErrorMessage("Successfully Deleted", 'success');
                        return true;
                    } else if (res.status >= 400) {
                        alert(res.data.message);
                        return false;
                    }
                }
                catch (e) {
                    throw e;
                }
            }, function (res) {
                var errorMessage = "Sorry some error occured";
                if (typeof (res.response.data) !== undefined) {
                    errorMessage = res.response.data.message;
                }
                that.props.onErrorMessage(errorMessage, 'error');
            });
        }
    },
    handleLinkNewAccount: function (e) {
        e.preventDefault();
        let fields = this.state.fields;
        var url = this.state.fields['url'];
        var site_id = this.state.fields['site_name'];
        if (!url || !site_id) {
            return;
        }
        this.state.fields['url'] = '';
        document.getElementById("site_name").value = '';
        var location = this.props.locId;
        var that = this;
        axios.defaults.headers.common['Authorization'] = cookies.get('token');
        axios.post(defaultURL + location + '/review_accounts', {
            url: url,
            site_id: site_id,
        }, {}).then(function (res) {
            try {

                if (res.status === 200 || res.status === 201) {
                    that.setState({reviewAccounts: res.data});
                    that.props.onErrorMessage("Successfully Added", 'success');
                    return true;
                } else if (res.status >= 400) {
                    return false;
                }
            }
            catch (e) {
                alert(res.data.message);
            }
        }, function (res) {
            var errorMessage = "Sorry some error occured";
            if (typeof (res.response.data) !== undefined) {
                errorMessage = res.response.data.message;
            }
            that.props.onErrorMessage(errorMessage, 'error');

        });
    },
    render: function () {
        var linkedAccounts = this.props.reviewAccounts.linked_accounts;
        var availableSites = this.props.reviewAccounts.available_sites;
        if (typeof(this.state.reviewAccounts.available_sites) !== "undefined") {
            linkedAccounts = this.state.reviewAccounts.linked_accounts;
            availableSites = this.state.reviewAccounts.available_sites;
            //this.state.reviewAccounts = {};
        }

        var linkedAccountsArr = [];
        linkedAccounts.map((linkedAccounts, index) => {
            linkedAccountsArr.push(linkedAccounts.site.id);
        })
        var renderAvailableSites = () => {
            if (availableSites.length > 0) {
                return availableSites.map((avSites, index) => {
                    return (
                        <option key={avSites.id} value={avSites.id}
                                disabled={(linkedAccountsArr.indexOf(avSites.id) === -1) ? "" : "disabled"}>{avSites.name}</option>
                    )
                })
            }
        }
        var RenderReviewAccounts = () => {
            if (linkedAccounts.length > 0) {
                return linkedAccounts.map((linkedAccounts, index) => {
                    return (
                        <tr key={linkedAccounts.site.id}>
                            <td>
                                <div className="row">
                                    <div className="col-lg-4">
                                        <img src={linkedAccounts.site.icon_url} className="social_icon"/>
                                    </div>
                                    <div className="col-lg-8">
                                        <span>{linkedAccounts.site.name}</span>
                                    </div>
                                </div>
                            </td>
                            <td className="team_email"><a target="_blank" href={linkedAccounts.url}>{linkedAccounts.url}</a></td>
                            <td><img src="images/Cross.png" className="change_cursor" onClick={this.deleteLinkedAccount.bind(this, linkedAccounts, index)}/></td>
                        </tr>
                    )
                })
            }
            return <tr>
                <td> No record found. Add new account.</td>
            </tr>
        };
        var RenderReviewAccountsMob = () => {
            if (linkedAccounts.length > 0) {
                return linkedAccounts.map((linkedAccounts, index) => {
                    return (
                        <div key={linkedAccounts.site.id}>

                                <div className="row">
                                    <div className="col-xs-2">
                                        <img src={linkedAccounts.site.icon_url} className="social_icon"/>
                                    </div>
                                    <div className="col-xs-8">
                                        <span>{linkedAccounts.site.name}</span>
                                    </div>
                                    <div className="col-xs-2">
                                        <img src="images/Cross.png" className="change_cursor" style={{'float':'right'}} onClick={this.deleteLinkedAccount.bind(this, linkedAccounts, index)}/>
                                    </div>
                                </div>
                            <div className="row">
                                <div className="col-xs-10" style={{'overflow':'hidden'}}>
                                    <a target="_blank" className="team_email" href={linkedAccounts.url}>{linkedAccounts.url}</a>
                                </div>
                            </div>
                            <hr/>
                        </div>


                    )
                })
            }
            return <div>
                <span> No record found. Add account now.</span>
            </div>
        };

        return (<div>
                <div className="row">
                    <div className="col-lg-8 all_reviews">

                        <div className="row">
                            <div className="col-lg-6">
                                <span className="all_review_head show-large">Link Your Accounts</span>
                            </div>
                            <div className="col-lg-6">
                                <div className="form_filter show-large">
                                    <button onClick={this.handleLinkAccount} className="btn btn_primary btn_add_team">
                                        Add Account
                                    </button>
                                    &emsp;
                                    <button className="btn btn_primary btn_filter" onClick={this.handleLinkNewAccount}>
                                        Save
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-sm-6">
                                <span className="all_review_head show-med">Link Your Accounts</span>
                            </div>
                            <div className="col-sm-6">
                                <div className="form_filter show-med">
                                    <button onClick={this.handleLinkAccount} className="btn btn_primary btn_add_team">
                                        Add Account
                                    </button>
                                    &emsp;
                                    <button className="btn btn_primary btn_filter" onClick={this.handleLinkNewAccount}>
                                        Save
                                    </button>
                                </div>
                            </div>
                        </div>

                        <span className="all_review_head show-mobile">Link Accounts</span>
                    </div>
                    <div className="col-lg-4">
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-12 show-mobile">
                        <div className="mob_align_center">
                            <button onClick={this.handleLinkAccount} className="btn btn_primary btn_add_team_m">Add
                                Account
                            </button>
                        </div>
                    </div>
                </div>
                <br className="show-mobile"/>
                <div className="row">
                    <div className="col-xs-12 show-mobile">
                        <div className="mob_align_center">
                            <button className="btn btn_primary btn_filter_m" onClick={this.handleLinkNewAccount}>Save
                            </button>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-8 table_box">

                        <div className=" table-responsive show-med">
                            <table id="acc_table_mob" className="table table-hover">
                                <thead>
                                <tr>
                                    <th>Site</th>
                                    <th>Page Link</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                {RenderReviewAccounts()}
                                <tr className={this.state.linkMutex ? "" : "invisi"}>
                                    <td>
                                        <div className="row">
                                            <div className="col-lg-12">
                                                <select id="site_name" name="site_name" ref="site_name"
                                                        className="edit_table small_edit"
                                                        onChange={this.handleTextChange.bind(this, "site_name")}>
                                                    <option key="" value="">Select Site</option>
                                                    {renderAvailableSites()}
                                                </select>
                                            </div>
                                        </div>

                                    </td>
                                    <td className="team_email">
                                        <input type="url" placeholder="www.AnyReviewSite.com"
                                               value={this.state.fields["url"]} ref="url"
                                               onChange={this.handleTextChange.bind(this, "url")}
                                               className="edit_table long_edit"
                                               maxLength="255"/>
                                    </td>
                                    <td></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                        <div className="show-mobile">
                            <span className="team_head">Site</span>
                            <span className="team_head" style={{'float':'right', 'margin':'0 0 0 10px'}}>Page Link</span>
                            <hr/>
                            {RenderReviewAccountsMob()}

                        </div>

                    </div>
                    <div className="col-lg-4">
                    </div>
                </div>
            </div>

        );
    }
});


var Account = React.createClass({
    getInitialState: function () {
        return {
            errorMessage: ""
        }
    },
    componentDidMount() {
        if (getClientRole(cookies.get("user"), cookies.get("selectedLocation")) === "member")
            browserHistory.push("/analytics")
    },
    handleErrorMessage: function (errorMessages, type) {
        this.props.handleErrorMessageInDashBoard(errorMessages, type);
    },
    render: function () {

        var {selectedLocation, membersJson, messageJson, locId, reviewAccounts} = this.props;
        if (membersJson === '' || typeof(membersJson) === undefined || messageJson === '' ||
            typeof(messageJson) === undefined || !checkNested(membersJson, 'members') || !checkNested(messageJson, 'data') ||
            !checkNested(reviewAccounts, 'available_sites') || !checkNested(reviewAccounts, 'linked_accounts')) {
            return (
                <div className="load_style"><img src="images/load_icon.gif"/></div>
            )
        }
        return (

            <div>

                <div className="row">
                    <div className="col-lg-12">
                        <div className="row">
                            <div className="col-lg-12">
                                <span className="all_review_head show-large"> Settings </span>
                            </div>
                        </div>
                        <br/>
                        <div className="row">
                            <div className="col-lg-12 bg_blu_m">
                                <Link reviewAccounts={reviewAccounts} locId={locId} onErrorMessage={this.handleErrorMessage}/>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

        )
    }
});

module.exports = Account;