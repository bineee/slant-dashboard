var React = require('react');
var Modal = require('react-modal');
var {Link, IndexLink} = require('react-router');
var axios = require('axios');

var LoginMessage = require('LoginMessage');

var LoginForm = React.createClass({
    getInitialState: function () {
        return this.state = {
            fields: {},
            errors: {},
            forgotEmailError:'',
        }
    },

    handleChange(field, e) {
        let fields = this.state.fields;
        fields[field] = e.target.value;
        this.setState({fields});
    },
    handleValidation() {
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;

        //Email
        if (!fields["email"]) {
            formIsValid = false;
            errors["email"] = "Username cannot be empty";
        }

        if (typeof fields["email"] !== "undefined") {
            let lastAtPos = fields["email"].lastIndexOf('@');
            let lastDotPos = fields["email"].lastIndexOf('.');

            if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["email"].indexOf('@@') == -1 && lastDotPos > 2 && (fields["email"].length - lastDotPos) > 2)) {
                formIsValid = false;
                errors["email"] = "Username is not valid";
            }
        }
        //password
        if (!fields["password"]) {
            formIsValid = false;
            errors["password"] = "Password cannot be empty";
        }

        if (typeof fields["password"] !== "undefined") {
            if (fields["password"].length < 6) {
                formIsValid = false;
                errors["password"] = "Password Cannot be less than 6 character";
            }

        }
        this.setState({errors: errors});
        return formIsValid;
    },

    onFormSubmit: function (e) {
        e.preventDefault();
        let fields = this.state.fields;
        var email = fields["email"];
        var password = fields["password"];
        if (this.handleValidation()) {
            this.props.onLoginButtonClicked(email, password);
            return true;
        } else {
            return false;
        }
    },
    render: function () {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-sm-4">

                    </div>
                    <div className="col-sm-4 box">
                        <div className="row">
                            <div className="col-sm-2 col-md-1">
                            </div>
                            <div className="col-sm-8 col-md-10 box_inside">
                                <form className="reviews_await" onSubmit={this.onFormSubmit}>
                                    Reviews await.
                                    <input type="text" placeholder="Username" id="log_user" className="log_user"
                                           onChange={this.handleChange.bind(this, "email")}
                                           value={this.state.fields["email"]} autoFocus required/>
                                    <span className="msg_error">{this.state.errors["email"]}</span>
                                    <input type="password" placeholder="Password" id="log_pass" className="log_pass"
                                           onChange={this.handleChange.bind(this, "password")}
                                           value={this.state.fields["password"]} required/>
                                    <span className="msg_error">{this.state.errors["password"]}</span>
                                    <button className="btn btn-primary btn_signin">
                                        <img src="images/airplane.png"/>&#8195; Sign In
                                    </button>
                                </form>
                                <br/>
                                <div className="col-sm-8 col-md-10">
                                    <Link to="/forgot">Forgot Your Password ?</Link>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-4">

                    </div>
                </div>
            </div>
        );
    }
});

module.exports = LoginForm;