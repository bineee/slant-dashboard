var React = require('react');
var {Link, IndexLink} = require('react-router');
import LocationDropDown from 'LocationDropDown';



var Cookies = require('universal-cookie');
var axios = require('axios');
const cookie = new Cookies();
var {Route, Router, IndexRoute, hashHistory, browserHistory} = require('react-router');
var Modal = require('react-modal');

var TopNav = React.createClass({
    getInitialState: function () {
        return this.state = {
            isActive: false,
            fields: {},
            errors: {},
        }

    },
    toggleModal: function () {
        let fields = this.state.fields;
        let errors = this.state.errors;
        fields["current"] = null;
        fields["new"] = null;
        fields["confirm"] = null;
        errors["current"] = '';
        errors["new"] = '';
        errors["confirm"] = '';
        this.setState({
            isActive: !this.state.isActive
        });
    },
    componentWillMount() {
        Modal.setAppElement('body');
    },
    handleLogout: function (e) {
        e.preventDefault();
        cookie.remove('selectedLocation');
        cookie.remove('token');
        cookie.remove('user');
        window.location.href = "/login";
        return;
    },
    handleLocationChange: function (location) {
        cookie.set("selectedLocation",location);
        this.props.onLocationChangeGraph(location);
    },
    handleErrorMessage(errorMessage, type) {
        this.props.handleErrorMessageInDashBoard(errorMessage, type);
    },
    handleChange(field, e) {
        let fields = this.state.fields;
        fields[field] = e.target.value;
        this.setState({fields});
    },
    handleValidation() {
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;

        /** Client Side Validation **/
        if (typeof fields["current"] !== "undefined") {
            if (!fields["current"]) {
                formIsValid = false;
                errors["current"] = "Current password cannot be empty";
            }
            else if (fields["current"].length < 6) {
                formIsValid = false;
                errors["current"] = "Current Password Cannot be less than 6 character";
            }
        }
        if (typeof fields["new"] !== "undefined" || typeof fields["confirm"] !== "undefined") {
            if (!fields["new"]) {
                formIsValid = false;
                errors["new"] = "New password cannot be empty";
            }

            else if (fields["new"].length < 6) {
                formIsValid = false;
                errors["new"] = "New Password Cannot be less than 6 character";
            }
            else if (!fields["confirm"]) {
                formIsValid = false;
                errors["confirm"] = "Confirm password cannot be empty";
            }
            else if (fields["confirm"].length < 6) {
                formIsValid = false;
                errors["confirm"] = "Confirm Password Cannot be less than 6 character";
            }
            else if (fields["new"] !== fields["confirm"]) {
                formIsValid = false;
                errors["confirm"] = "Password Mismatch";
            }

        }
        this.setState({errors: errors});
        return formIsValid;
    },

    onFormSubmit: function (e) {
        e.preventDefault();
        let fields = this.state.fields;
        var currentP = fields["current"];
        var newP = fields["new"];
        if (this.handleValidation()) {
            this.setState({
                isActive: !this.state.isActive
            });

            var that = this;
            const user_id = cookie.get('user').id;
            const defaultURL = 'https://api.slantreviews.com/v2/users/';
            axios.defaults.headers.common['Authorization'] = cookie.get('token');
            axios.post(defaultURL + user_id + '/password ', {
                current_password: currentP,
                new_password: newP
            }, {}).then(function (res) {
                try {
                    if (res.status === 200) {
                        that.handleErrorMessage("Password Change Successfully", 'success');
                        that.setState({
                            access_token: res.data.access_token,
                            isLoading: false
                        });
                        cookie.set('token', res.data.access_token, {path: '/'});
                        e.preventDefault();
                        return true;
                    } else {
                        that.handleErrorMessage(res.data.message, 'error');
                        return false;
                    }
                }
                catch (e) {
                    throw e;
                }

            }, function (res) {
                var errorMessage = "Sorry some error occured";
                if (typeof (res.response.data) !== undefined) {
                    errorMessage = res.response.data.message;
                }
                that.handleErrorMessage(errorMessage, 'error');
            });
            fields["current"] = '';
            fields["new"] = '';
            fields["confirm"] = '';
        } else {
            return false;
        }
    },
    render: function () {
        var {locationArray} = this.props;
        return (
            <div className="headbar">
                <div className="row">
                    <div className="col-lg-6 col-xs-6">
                        <LocationDropDown locationArray={locationArray}
                                          onLocationChangeStat={this.handleLocationChange} />
                    </div>
                    <div className="col-lg-6 col-xs-6">
                        <div className="navbar-right slant-logout">

                            <img src="images/CustomerProfile.png" className="btn dropdown-toggle usr_logo"
                                 data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="dropdownMenu11"/>

                            <ul className="dropdown-menu usr_dd" aria-labelledby="dropdownMenu11">
                                <li><Link onClick={this.toggleModal}>Change Password</Link></li>
                                <li><Link to="#" onClick={this.handleLogout}>Logout</Link></li>
                            </ul>
                        </div>
                    </div>

                    <Modal
                        isOpen={this.state.isActive}
                        onRequestClose={this.toggleModal}
                        contentLabel="Example Modal"
                        className="box1">

                        <div className="container">
                            <div className="row">
                                <div className="col-lg-4">

                                </div>
                                <div className="col-lg-4 col-xs-12 box">
                                    <div className="row">
                                        <div className="col-lg-2 col-xs-1">
                                        </div>
                                        <div className="col-lg-8 col-xs-10 box_inside">
                                            <form className="reviews_await" onSubmit={this.onFormSubmit}>
                                                Account Setting
                                                <br/>
                                                <br/>
                                                <input type="password" placeholder="Current password"
                                                       className="log_user"
                                                       onChange={this.handleChange.bind(this, "current")}
                                                       value={this.state.fields["current"]}
                                                       autoFocus required autoComplete="false"/>
                                                <span className="msg_error">{this.state.errors["current"]}</span>

                                                <input type="password" placeholder="New password" className="log_user"
                                                       onChange={this.handleChange.bind(this, "new")}
                                                       value={this.state.fields["new"]}
                                                       required/>
                                                <span className="msg_error">{this.state.errors["new"]}</span>

                                                <input type="password" placeholder="Confirm password"
                                                       className="log_user"
                                                       onChange={this.handleChange.bind(this, "confirm")}
                                                       value={this.state.fields["confirm"]}
                                                       required/>
                                                <span className="msg_error">{this.state.errors["confirm"]}</span>
                                                <span className="msg_error">{this.state.errors["number"]}</span>
                                                <button className="btn btn_primary btn_signin">
                                                    <img src="images/airplane.png"/>&#8195;
                                                    Change password
                                                </button>

                                            </form>
                                        </div>
                                        <div className="col-lg-2 col-xs-1">
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-4">

                                </div>
                            </div>
                        </div>
                    </Modal>
                </div>
            </div>

        );
    }
});

module.exports = TopNav;
