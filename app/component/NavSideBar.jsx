var React = require('react');
var {Link, IndexLink} = require('react-router');
var Modal = require('react-modal');
var axios = require('axios');
var Cookies = require('universal-cookie');
var invitationApi = require('invitationApi');
import LocationDropDown from 'LocationDropDown';
const cookies = new Cookies();
const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)'
    }
};


var NavSideBar = React.createClass({
    getInitialState: function () {
        return this.state = {
            isActive: false,
            fields: {},
            errors: {},
            customerJson: this.props.customerJson,
            clickCondition: false,
            revDD: false,
            setDD: false,
            customerCount: null,
            client_role : ''
        }

    },

    handleClick: function () {
        this.setState({
            clickCondition: !this.state.clickCondition
        });
    },
    handleDD: function () {
        this.setState({
            revDD: !this.state.revDD
        });
    },
    handleDD2: function () {
        this.setState({
            setDD: !this.state.setDD
        });
    },

    handleNumber(field, e) {
        var charCode = e.keyCode || e.which;
        try {
           if ((charCode >= 48 && charCode <= 57) || charCode === 45 || charCode=== 40 || charCode === 41 || charCode ===43)
                return true;
            else if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                e.preventDefault();
                return false;
            }
        }
        catch (err) {
            alert(err.Description);
        }

    },

    handleText(field, e) {
        var charCode = e.keyCode || e.which;
            if ((charCode >= 65 && charCode <=90) || (charCode>=97 && charCode>=22) || charCode==8 || charCode==32)   {
                return true;
            }
            else {
                e.preventDefault();
                return false;
            }
        /*}
        catch (err) {
            alert(err.Description);
        }*/

    },
    handleChange(field, e) {
        let fields = this.state.fields;
        fields[field] = e.target.value;
        this.setState({fields});
    },
    handleErrorMessage(errorMessage, type){
        this.props.handleErrorMessageInDashBoard(errorMessage, type);
    },

    handleValidation() {
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;

        /**Client Side Validation for Phone Number*/
        if (!fields["name"]) {
            formIsValid = false;
            errors["name"] = "First name cannot be empty";
        }

        if (typeof fields["name"] !== "undefined") {
            if (fields["name"].length > 40) {
                formIsValid = false;
                errors["name"] = "First Name Cannot be more than 40 characters";
            }
        }

        if (!fields["number"]) {
            formIsValid = false;
            errors["number"] = "Phone Number cannot be empty";
        }

        if (typeof fields["number"] !== "undefined") {
            if(fields["number"].match(/\d+/g).toString().length < 10){
                formIsValid = false;
                errors["number"] = "Phone Number should not be less than 10 digit";
            }else if (fields["number"].match(/\d+/g).toString().length > 15 ) {
                formIsValid = false;
                errors["number"] = "Phone Number should not be more than 15 digit and less than 10 digit";
            }
        }
        this.setState({errors: errors});
        return formIsValid;
    },
    componentWillMount() {
        Modal.setAppElement('body');
    },
    toggleModal: function () {
        let fields = this.state.fields;
        if($(".box1")[0]){
            $(".ReactModal__Overlay ").css("background-color", 'rgba(255,255,255,0')
        }
        $(".box1").remove();
        this.setState({
            isActive: !this.state.isActive
        });
        fields["name"] = '';
        fields["number"] = '';
    },
    onFormSubmit: function (e) {
        e.preventDefault();
        let fields = this.state.fields;

        var customerArray = this.props.customerJson;
        var name = fields["name"];
        var number = fields["number"];

        if (this.handleValidation()) {
            this.setState({
                isActive: !this.state.isActive
            });
            var that = this;
            var c = customerArray.review_invitations.push({
                client_name: name,
                client_number_natural: number,
                date_sent: new Date(),
                recommended: null,
            });
            that.setState({customerJson: c});
            that.setState({customerCount: customerArray.review_invitations.length});
            var location = this.props.locId;
            const defaultURL = 'https://api.slantreviews.com/v2/locations/';
            axios.defaults.headers.common['Authorization'] = cookies.get('token');
            axios.post(defaultURL + location + '/review_invitations', {
                to_number: number,
                to_name: name
            }, {}).then(function (res) {
                try {
                    if (res.status === 201) {

                        fields["name"] = '';
                        fields["number"] = '';
                        //alert(res.data.message);
                        that.props.updateCustomerCount(customerArray.review_invitations.length);
                        that.handleErrorMessage("Invitation sent successfully", 'success');
                        e.preventDefault();

                        return true;
                    } else {
                        alert(res.data.message);
                        fields["name"] = '';
                        fields["number"] = '';
                        return false;
                    }
                }
                catch (e) {
                    throw e;
                }

            },function (res) {
                var errorMessage = "Sorry some error occured";
                if(typeof (res.response.data) !== undefined) {
                    errorMessage = res.response.data.message;
                }
                that.handleErrorMessage(errorMessage, type);
                //that.props.onErrorMessage(errorMessage, 'error')
            });
            //this.props.onInvitationSent(c);

        } else {
            return false;
        }
    },
    toggleSidenav: function () {
        var css = (this.props.showHideSidenav === " ") ? "active" : " ";
        this.setState({"showHideSidenav": css});
    },


    render: function () {
        var clientRole = getClientRole(cookies.get('user'), cookies.get("selectedLocation"));
        var totCustomer = 0;
        if (this.state.customerCount != null) {
            totCustomer = this.state.customerCount;
            this.state.customerCount = null;
        } else {
            totCustomer = this.props.customerCount;
        }
        var reviewData = this.props.reviewsJson;
        var selectedLocation = this.props.locId;
        var customerJson = this.props.customerJson;
        var total = 0;
        var customerCount = 0;
        if (!jQuery.isEmptyObject(reviewData) || !typeof(reviewsJson) === "undefined") {
            total = reviewData.reviews.length;
        }


        var renderReviewDD1 = () => {
            if (this.state.revDD === true) {
                return (

                    <Link to={{ pathname: '/review', query: { q: 'online' } }} activeClassName="active">
                        &#8195;&#8195;Online Reviews
                    </Link>
                );
            }
            return <div></div>
        };
        var renderReviewDD2 = () => {
            if (this.state.revDD === true) {
                return (
                    <Link to={{ pathname: '/review', query: { q: 'private' } }} activeClassName="active">
                        &#8195;&#8195; Private Feedback
                    </Link>
                );

            }
            return <div></div>
        };
        var renderSettingDD1 = () => {
            if (this.state.setDD === true) {
                return (
                    <Link to={{ pathname: '/settings', query: { q: 'link' } }} activeClassName="active">
                        &#8195;&#8195; Link Accounts
                    </Link>
                );

            }
            return <div></div>
        };
        var renderSettingDD2 = () => {
            if (this.state.setDD === true) {
                return (
                    <Link to={{ pathname: '/settings', query: { q: 'team' } }} activeClassName="active">
                        &#8195;&#8195; Team Management
                    </Link>
                );

            }
            return <div></div>
        };
        var renderSettingDD3 = () => {
            if (this.state.setDD === true) {
                return (
                    <Link to={{ pathname: '/settings', query: { q: 'sms' } }} activeClassName="active">
                        &#8195;&#8195; Customize SMS
                    </Link>
                );

            }
            return <div></div>
        };

        return (
            <div>
                <div className=" side_nav">

                    <div className="row">
                        <div className="col-lg-12 show-large">

                            <br/>
                            <div className="logo_holder">
                                Logo
                            </div>

                            <br/>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-lg-12 show-large">
                            <button onClick={this.toggleModal} className="btn btn_primary send_invite">
                                <img src="images/airplane.png"/>&#8195;  Send Request
                            </button>
                        </div>
                    </div>
                    <br/>

                    <div className="navbar nav-pills nav-stacked" id="my-navbar">

                        <div className="navbar-header">

                            <div className="row">
                                <div className="col-lg-12">
                                    <button id="nav-icon1" type="button" onClick={this.handleClick}
                                            className={this.state.clickCondition ? "navbar-toggle open" : "navbar-toggle"}
                                            data-toggle="collapse" data-target="#navbar-collapse">
                                        <span className="icon-bar tog_btn "></span>
                                        <span className="icon-bar tog_btn"></span>
                                        <span className="icon-bar tog_btn"></span>
                                    </button>

                                    <div className="logo_holder show-mobile navbar-brand" style={{'margin-left': '20px'}}>
                                        Logo
                                    </div>
                                    <div className="logo_holder show-med navbar-brand" style={{'margin-left': '20px'}}>
                                        Logo
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="collapse navbar-collapse" id="navbar-collapse">
                            <ul className="naav nav-pills nav-stacked">
                                <br/>
                                <br/>
                                <li>
                                    <Link to="/analytics" activeClassName="active">

                                        <svg width="15px" className="n_icon" height="15px" viewBox="0 0 15 15" version="1.1" xmlns="http://www.w3.org/2000/svg">

                                            <title>Dashboard Icon Copy</title>
                                            <desc>Created with Sketch.</desc>
                                            <defs></defs>
                                            <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <g id="Analytics" transform="translate(-23.000000, -205.000000)" stroke="#979797">
                                                    <g id="Dashboard-Icon-Copy" transform="translate(23.000000, 205.000000)">
                                                        <rect id="Rectangle-9" x="0.5" y="8.5" width="2" height="6" rx="1"></rect>
                                                        <rect id="Rectangle-9-Copy" x="6.5" y="4.5" width="2" height="9" rx="1"></rect>
                                                        <rect id="Rectangle-9-Copy-2" x="12.5" y="0.5" width="2" height="13" rx="1"></rect>
                                                    </g>
                                                </g>
                                            </g>
                                        </svg>
                                        &#8195; Analytics
                                    </Link>
                                </li>

                                <li className="show-large">
                                    <Link to="/review" activeClassName="active">
                                        <svg width="15px" className="n_icon" height="15px" viewBox="0 0 21 20" version="1.1" xmlns="http://www.w3.org/2000/svg">

                                            <title>Review Icon</title>
                                            <desc>Created with Sketch.</desc>
                                            <defs></defs>
                                            <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
                                                <g id="Analytics" transform="translate(-21.000000, -259.000000)" stroke="#B2B2BE" fill-rule="nonzero" stroke-width="2">
                                                    <polygon id="Review-Icon" points="31.2150002 274.486028 25.5198169 277.527972 26.6529688 271.171518 22 266.69509 28.3955097 265.808529 31.2150002 260 34.0344906 265.808529 40.4300003 266.69509 35.7770315 271.171518 36.9101835 277.527972"></polygon>
                                                </g>
                                            </g>
                                        </svg>
                                        &#8195; Reviews &nbsp;
                                        <span className="badge custom_badge">{total}</span>
                                    </Link>
                                </li>
                                <li>
                                    <div className={(window.location.href.toString().indexOf("review?q") !== -1) ? "show-mobile revDD blu" : "show-mobile revDD"} onClick={this.handleDD}>
                                        <svg width="15px" className="n_icon" height="15px" viewBox="0 0 21 20" version="1.1" xmlns="http://www.w3.org/2000/svg">

                                            <title>Review Icon</title>
                                            <desc>Created with Sketch.</desc>
                                            <defs></defs>
                                            <g id="Page-1" stroke={(window.location.href.toString().indexOf("review?q") !== -1) ? "#3E41FF" : "none"}
                                               stroke-width="1" fill={(window.location.href.toString().indexOf("review?q") !== -1) ? "#3E41FF" : "none"} fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
                                                <g id="Analytics" transform="translate(-21.000000, -259.000000)" stroke="#B2B2BE" fill-rule="nonzero" stroke-width="2">
                                                    <polygon id="Review-Icon" points="31.2150002 274.486028 25.5198169 277.527972 26.6529688 271.171518 22 266.69509 28.3955097 265.808529 31.2150002 260 34.0344906 265.808529 40.4300003 266.69509 35.7770315 271.171518 36.9101835 277.527972"></polygon>
                                                </g>
                                            </g>
                                        </svg>
                                        &#8195; Reviews &nbsp;
                                        <span className="badge custom_badge">{total}</span>
                                        <span className="DDarrow"><img src="images/Downarrow.png"/> </span>
                                    </div>
                                </li>

                                <li className="show-mobile">
                                    {renderReviewDD1()}
                                </li>
                                <li className="show-mobile">
                                    {renderReviewDD2()}
                                </li>

                                <li>
                                    <div className={(window.location.href.toString().indexOf("review?q") !== -1) ? "show-med revDD blu" : "show-med revDD"} onClick={this.handleDD}>
                                        <svg width="15px" className="n_icon" height="15px" viewBox="0 0 21 20" version="1.1" xmlns="http://www.w3.org/2000/svg">

                                            <title>Review Icon</title>
                                            <desc>Created with Sketch.</desc>
                                            <defs></defs>
                                            <g id="Page-1" stroke={(window.location.href.toString().indexOf("review?q") !== -1) ? "#3E41FF" : "none"}
                                               stroke-width="1" fill={(window.location.href.toString().indexOf("review?q") !== -1) ? "#3E41FF" : "none"}
                                               fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
                                                <g id="Analytics" transform="translate(-21.000000, -259.000000)" stroke="#B2B2BE" fill-rule="nonzero" stroke-width="2">
                                                    <polygon id="Review-Icon" points="31.2150002 274.486028 25.5198169 277.527972 26.6529688 271.171518 22 266.69509 28.3955097 265.808529 31.2150002 260 34.0344906 265.808529 40.4300003 266.69509 35.7770315 271.171518 36.9101835 277.527972"></polygon>
                                                </g>
                                            </g>
                                        </svg>
                                        &#8195; Reviews &nbsp;
                                        <span className="badge custom_badge">{total}</span>
                                        <span className="DDarrow"><img src="images/Downarrow.png"/> </span>
                                    </div>
                                </li>

                                <li className="show-med">
                                    {renderReviewDD1()}
                                </li>
                                <li className="show-med">
                                    {renderReviewDD2()}
                                </li>

                                <li>
                                    <Link to="/customer" activeClassName="active">
                                        <svg width="15px" className="n_icon" height="15px" viewBox="0 0 16 19" version="1.1" xmlns="http://www.w3.org/2000/svg">

                                            <title>Customer Icon</title>
                                            <desc>Created with Sketch.</desc>
                                            <defs></defs>
                                            <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round">
                                                <g id="Analytics" transform="translate(-23.000000, -315.000000)" stroke="#B2B2BE" stroke-width="2">
                                                    <g id="Customer-Icon" transform="translate(24.000000, 316.000000)">
                                                        <path d="M14,17 C14,13.1340068 10.8659932,10 7,10 C3.13400675,10 0,13.1340068 0,17" id="Oval-8"></path>
                                                        <circle id="Oval-8-Copy" cx="7" cy="5.40002441" r="4.5"></circle>
                                                    </g>
                                                </g>
                                            </g>
                                        </svg>
                                        &#8195; Customers
                                        <span className="badge custom_badge">{totCustomer}</span>
                                    </Link>
                                </li>
                                <li className="show-large" style={{"display":(clientRole === "admin")?"":"none"}}>
                                    <Link to={"/settings"} activeClassName="active">
                                        <svg width="15px" className="n_icon" height="15px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" >

                                            <title>settings icon</title>
                                            <desc>Created with Sketch.</desc>
                                            <defs></defs>
                                            <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <g id="Analytics" transform="translate(-19.000000, -370.000000)">
                                                    <g id="settings-icon" transform="translate(19.000000, 370.000000)">
                                                        <circle id="Oval-10" stroke="#B2B2BE" stroke-width="2" cx="12" cy="12" r="8"></circle>
                                                        <circle id="Oval-10" fill="#B2B2BE" cx="12" cy="12" r="2"></circle>
                                                        <g id="Group-6" transform="translate(11.000000, 0.000000)" stroke="#B2B2BE">
                                                            <rect id="Rectangle-4" x="0.5" y="0.5" width="1" height="3" rx="0.5"></rect>
                                                            <rect id="Rectangle-4" x="0.5" y="20.5" width="1" height="3" rx="0.5"></rect>
                                                        </g>
                                                        <g id="Group-6" transform="translate(12.000000, 12.000000) rotate(-270.000000) translate(-12.000000, -12.000000) translate(11.000000, 0.000000)" stroke="#B2B2BE">
                                                            <rect id="Rectangle-4" x="0.5" y="0.5" width="1" height="3" rx="0.5"></rect>
                                                            <rect id="Rectangle-4" x="0.5" y="20.5" width="1" height="3" rx="0.5"></rect>
                                                        </g>
                                                        <g id="Group-6" transform="translate(12.000000, 12.000000) rotate(-315.000000) translate(-12.000000, -12.000000) translate(11.000000, 0.000000)" stroke="#B2B2BE">
                                                            <rect id="Rectangle-4" x="0.5" y="0.5" width="1" height="3" rx="0.5"></rect>
                                                            <rect id="Rectangle-4" x="0.5" y="20.5" width="1" height="3" rx="0.5"></rect>
                                                        </g>
                                                        <g id="Group-6" transform="translate(12.000000, 12.000000) rotate(-225.000000) translate(-12.000000, -12.000000) translate(11.000000, 0.000000)" stroke="#B2B2BE">
                                                            <rect id="Rectangle-4" x="0.5" y="0.5" width="1" height="3" rx="0.5"></rect>
                                                            <rect id="Rectangle-4" x="0.5" y="20.5" width="1" height="3" rx="0.5"></rect>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </svg>
                                        &#8195; Settings
                                    </Link>
                                </li>

                                <li style={{'display':(getClientRole(cookies.get("user"), cookies.get("selectedLocation")) === "member")? 'none': ''}}>

                                    <div className={(window.location.href.toString().indexOf("settings?q") !== -1) ? "show-mobile revDD blu" : "show-mobile revDD"} onClick={this.handleDD2}>
                                        <svg width="15px" className="n_icon" height="15px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" >

                                            <title>settings icon</title>
                                            <desc>Created with Sketch.</desc>
                                            <defs></defs>
                                            <g id="Page-1" stroke={(window.location.href.toString().indexOf("settings?q") !== -1) ? "#3E41FF" : "none"}
                                               stroke-width="1" fill={(window.location.href.toString().indexOf("settings?q") !== -1) ? "#3E41FF" : "none"}
                                               fill-rule="evenodd">
                                                <g id="Analytics" transform="translate(-19.000000, -370.000000)">
                                                    <g id="settings-icon" transform="translate(19.000000, 370.000000)">
                                                        <circle id="Oval-10" stroke="#B2B2BE" stroke-width="2" cx="12" cy="12" r="8"></circle>
                                                        <circle id="Oval-10" fill="#B2B2BE" cx="12" cy="12" r="2"></circle>
                                                        <g id="Group-6" transform="translate(11.000000, 0.000000)" stroke="#B2B2BE">
                                                            <rect id="Rectangle-4" x="0.5" y="0.5" width="1" height="3" rx="0.5"></rect>
                                                            <rect id="Rectangle-4" x="0.5" y="20.5" width="1" height="3" rx="0.5"></rect>
                                                        </g>
                                                        <g id="Group-6" transform="translate(12.000000, 12.000000) rotate(-270.000000) translate(-12.000000, -12.000000) translate(11.000000, 0.000000)" stroke="#B2B2BE">
                                                            <rect id="Rectangle-4" x="0.5" y="0.5" width="1" height="3" rx="0.5"></rect>
                                                            <rect id="Rectangle-4" x="0.5" y="20.5" width="1" height="3" rx="0.5"></rect>
                                                        </g>
                                                        <g id="Group-6" transform="translate(12.000000, 12.000000) rotate(-315.000000) translate(-12.000000, -12.000000) translate(11.000000, 0.000000)" stroke="#B2B2BE">
                                                            <rect id="Rectangle-4" x="0.5" y="0.5" width="1" height="3" rx="0.5"></rect>
                                                            <rect id="Rectangle-4" x="0.5" y="20.5" width="1" height="3" rx="0.5"></rect>
                                                        </g>
                                                        <g id="Group-6" transform="translate(12.000000, 12.000000) rotate(-225.000000) translate(-12.000000, -12.000000) translate(11.000000, 0.000000)" stroke="#B2B2BE">
                                                            <rect id="Rectangle-4" x="0.5" y="0.5" width="1" height="3" rx="0.5"></rect>
                                                            <rect id="Rectangle-4" x="0.5" y="20.5" width="1" height="3" rx="0.5"></rect>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </svg>
                                        &#8195; Settings
                                        <span className="DDarrow"><img src="images/Downarrow.png"/> </span>
                                    </div>

                                </li>
                                <li className="show-mobile">
                                    {renderSettingDD1()}
                                </li>
                                <li className="show-mobile">
                                    {renderSettingDD2()}
                                </li>
                                <li className="show-mobile">
                                    {renderSettingDD3()}
                                </li>

                                <li style={{'display':(getClientRole(cookies.get("user"), cookies.get("selectedLocation")) === "member")? 'none': ''}}>

                                    <div className={(window.location.href.toString().indexOf("settings?q") !== -1) ? "show-med revDD blu" : "show-med revDD"} onClick={this.handleDD2}>
                                        <svg width="15px" className="n_icon" height="15px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" >

                                            <title>settings icon</title>
                                            <desc>Created with Sketch.</desc>
                                            <defs></defs>
                                            <g id="Page-1" stroke={(window.location.href.toString().indexOf("settings?q") !== -1) ? "#3E41FF" : "none"}
                                               stroke-width="1" fill={(window.location.href.toString().indexOf("settings?q") !== -1) ? "#3E41FF" : "none"}
                                               fill-rule="evenodd">
                                                <g id="Analytics" transform="translate(-19.000000, -370.000000)">
                                                    <g id="settings-icon" transform="translate(19.000000, 370.000000)">
                                                        <circle id="Oval-10" stroke="#B2B2BE" stroke-width="2" cx="12" cy="12" r="8"></circle>
                                                        <circle id="Oval-10" fill="#B2B2BE" cx="12" cy="12" r="2"></circle>
                                                        <g id="Group-6" transform="translate(11.000000, 0.000000)" stroke="#B2B2BE">
                                                            <rect id="Rectangle-4" x="0.5" y="0.5" width="1" height="3" rx="0.5"></rect>
                                                            <rect id="Rectangle-4" x="0.5" y="20.5" width="1" height="3" rx="0.5"></rect>
                                                        </g>
                                                        <g id="Group-6" transform="translate(12.000000, 12.000000) rotate(-270.000000) translate(-12.000000, -12.000000) translate(11.000000, 0.000000)" stroke="#B2B2BE">
                                                            <rect id="Rectangle-4" x="0.5" y="0.5" width="1" height="3" rx="0.5"></rect>
                                                            <rect id="Rectangle-4" x="0.5" y="20.5" width="1" height="3" rx="0.5"></rect>
                                                        </g>
                                                        <g id="Group-6" transform="translate(12.000000, 12.000000) rotate(-315.000000) translate(-12.000000, -12.000000) translate(11.000000, 0.000000)" stroke="#B2B2BE">
                                                            <rect id="Rectangle-4" x="0.5" y="0.5" width="1" height="3" rx="0.5"></rect>
                                                            <rect id="Rectangle-4" x="0.5" y="20.5" width="1" height="3" rx="0.5"></rect>
                                                        </g>
                                                        <g id="Group-6" transform="translate(12.000000, 12.000000) rotate(-225.000000) translate(-12.000000, -12.000000) translate(11.000000, 0.000000)" stroke="#B2B2BE">
                                                            <rect id="Rectangle-4" x="0.5" y="0.5" width="1" height="3" rx="0.5"></rect>
                                                            <rect id="Rectangle-4" x="0.5" y="20.5" width="1" height="3" rx="0.5"></rect>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </svg>
                                        &#8195; Settings
                                        <span className="DDarrow"><img src="images/Downarrow.png"/> </span>
                                    </div>

                                </li>
                                <li className="show-med">
                                    {renderSettingDD1()}
                                </li>
                                <li className="show-med">
                                    {renderSettingDD2()}
                                </li>
                                <li className="show-med">
                                    {renderSettingDD3()}
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <Modal
                    isOpen={this.state.isActive}
                    onRequestClose={this.toggleModal}
                    contentLabel="Example Modal"
                    className="box">

                    <div className="container">
                        <div className="row">
                            <div className="col-lg-4">

                            </div>
                            <div className="col-lg-4 col-xs-12 box">
                                <div className="row">
                                    <div className="col-lg-2 col-xs-1">
                                    </div>
                                    <div className="col-lg-8 col-xs-10 box_inside">
                                        <form className="reviews_await" onSubmit={this.onFormSubmit}>
                                            Send a review request to
                                            <br/>
                                            your happy customers.
                                            <br/>
                                            <br/>
                                            <input type="text" onKeyPress={this.handleText.bind(this,"name")}
                                                   placeholder="First Name" className="log_user" onChange={this.handleChange.bind(this, "name")}
                                                   value={this.state.fields["name"]} required autoFocus
                                                  />
                                            <span style={{
                                                color: "red",
                                                fontSize: "17px"
                                            }}>{this.state.errors["name"]}</span>

                                            <input type="tel" onKeyPress={this.handleNumber.bind(this, "number")}
                                                   placeholder="Phone Number" className="log_pass"
                                                   onChange={this.handleChange.bind(this, "number")}
                                                   value={this.state.fields["number"]} required
                                                   />
                                            <span style={{
                                                color: "red",
                                                fontSize: "17px"
                                            }}>{this.state.errors["number"]}</span>
                                            <button className="btn btn_primary btn_signin">
                                                <img src="images/airplane.png"/>&#8195;  Send Request
                                            </button>

                                        </form>
                                    </div>
                                    <div className="col-lg-2 col-xs-1">
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-4">

                            </div>
                        </div>
                    </div>
                </Modal>
                <button id="float_send" type="button" onClick={this.toggleModal} className="right_btn show-med"><img src="images/airplane.png"/></button>

                <button id="float_send" type="button" onClick={this.toggleModal} className="right_btn show-mobile"><img src="images/airplane.png"/></button>
            </div>

        );
    }
});

module.exports = NavSideBar;
