var axios = require('axios');
var Cookies = require('universal-cookie');
var {Route, Router, IndexRoute, hashHistory, browserHistory} = require('react-router');
const defaultURL = 'https://api.slantreviews.com/v2/users/me';
const cookies = new Cookies();
module.exports = {

    getCurrentUser: function (token) {
       return axios.get(defaultURL,{ headers: { Authorization: token } }).then(function (res) {
            if(res.status != 200){
                throw new Error(res.data.message);
            }else{
                return res.data;
            }

        },function (res) {
            throw new Error(res.response.message);
        });
    }
}
